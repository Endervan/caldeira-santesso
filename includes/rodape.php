<div class="clearfix"></div>
<div class="container-fluir fundo-preto top40">
	<div class="row">
	<!-- MENU RODAPE -->
		<div class="container">
			<div class="row">
				<div class="col-xs-9 top10">
					<ul class="menu-rodape">
						<li><a href="<?php echo Util::caminho_projeto() ?>/">HOME</a></li>
						<li><a href="<?php echo Util::caminho_projeto() ?>/empresa">A EMPRESA</a></li>
						<li><a href="<?php echo Util::caminho_projeto() ?>/servicos">SERVIÇOS</a></li>
						<li><a href="<?php echo Util::caminho_projeto() ?>/produtos">PRODUTOS</a></li>
						<li><a href="<?php echo Util::caminho_projeto() ?>/especificacoes">ESPECIFICAÇÕES</a></li>
						<li><a href="<?php echo Util::caminho_projeto() ?>/contato">CONTATO</a></li>
					</ul>
				</div>
				<div class="col-xs-3 text-right top20">

					<?php if ($config[google_plus] != "") { ?> 
						 <a class="pull-left top25 left170" href="<?php Util::imprime($config[google_plus]); ?>" title="Google Plus" target="_blank" > 
							 <p class=""><i style="color: #fff" class="fa fa-google-plus"></i></p> 
						 </a> 
					 <?php } ?>

					<a href="http://www.homewebbrasil.com.br" target="_blank">
						<img src="<?php echo Util::caminho_projeto() ?>/imgs/logo-masmidia.png" alt="">
					</a>
				</div>
				<div class="col-xs-6 left10">
					<div class="contatos-rodape top10">
						<img src="<?php echo Util::caminho_projeto() ?>/imgs/logo-home.png" alt="">
						<span><?php Util::imprime($config[endereco]) ?></span>
					</div>
					<div class="contatos-rodape ">
						<img src="<?php echo Util::caminho_projeto() ?>/imgs/icon-phone.png" alt="">
						<span>
							<?php Util::imprime($config[telefone1]) ?>
							<?php if($config[telefone2] != ''){ Util::imprime(' / ' .$config[telefone2]); }  ?>
							<?php if($config[telefone3] != ''){ Util::imprime(' / ' .$config[telefone3]); }  ?>
							<?php if($config[telefone4] != ''){ Util::imprime(' / ' .$config[telefone4]); }  ?>
						</span>
					</div>
				</div>
			</div>
		</div>
		<!-- MENU RODAPE -->
	</div>
</div>


<div class="container-fluir fundo-laranja text-center">
	<div class="row">
		<h1>TODOS OS DIREITOS RESERVADOS</h1>
	</div>
</div>	