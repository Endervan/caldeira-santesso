<div class="container">
  <div class="row">
    <div class="col-xs-9">
        <h1 class="topo-telefone pull-right" style="margin-top: 0px;"><img src="<?php echo Util::caminho_projeto() ?>/imgs/telefones-topo.png" alt=""><span><?php Util::imprime($config[telefone1]) ?> <?php if($config[telefone2] != ''){ Util::imprime(' / ' .$config[telefone2]); }  ?> </span> </h1>
    </div>
    <div class="col-xs-3">
        <div class="dropdown orcamentos">
          <a id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn-meu-orcamento">
             <span class="badge"><?php echo count($_SESSION[solicitacoes_produtos]) + count($_SESSION[solicitacoes_servicos]) ?></span>
            <img src="<?php echo Util::caminho_projeto() ?>/imgs/seta-menu-topo.png" height="18" width="29" alt="">
          </a>

          <div class="dropdown-menu topo-meu-orcamento pull-right" aria-labelledby="dLabel">
            <h6 class="bottom20">MEU ORÇAMENTO(<?php echo count($_SESSION[solicitacoes_produtos]) + count($_SESSION[solicitacoes_servicos]) ?>)</h6>
            

            <?php
            if(count($_SESSION[solicitacoes_produtos]) > 0)
            {
                for($i=0; $i < count($_SESSION[solicitacoes_produtos]); $i++)
                {
                    $row = $obj_site->select_unico("tb_produtos", "idproduto", $_SESSION[solicitacoes_produtos][$i]);
                    ?>
                    <div class="lista-itens-carrinho">
                        <div class="col-xs-3">
                            <img src="<?php echo Util::caminho_projeto() ?>/uploads/tumb_<?php Util::imprime($row[imagem]) ?>" class="input100" alt="">    
                        </div>
                        <div class="col-xs-7">
                            <h1><?php Util::imprime($row[titulo]) ?></h1>
                        </div>
                        <div class="col-xs-2 top30">
                            <a href="<?php echo Util::caminho_projeto() ?>/orcamento/?action=del&id=<?php echo $i; ?>&tipo=produto" data-toggle="tooltip" data-placement="top" title="Excluir"> <i class="glyphicon glyphicon-remove"></i> </a>
                        </div>
                    </div>
                    <?php  
                }
            }
            ?>




            <?php
            if(count($_SESSION[solicitacoes_servicos]) > 0)
            {
                for($i=0; $i < count($_SESSION[solicitacoes_servicos]); $i++)
                {
                    $row = $obj_site->select_unico("tb_servicos", "idservico", $_SESSION[solicitacoes_servicos][$i]);
                    ?>
                    <div class="lista-itens-carrinho">
                        <div class="col-xs-3">
                            <img src="<?php echo Util::caminho_projeto() ?>/uploads/tumb_<?php Util::imprime($row[imagem]) ?>" class="input100" alt="">    
                        </div>
                        <div class="col-xs-7">
                            <h1><?php Util::imprime($row[titulo]) ?></h1>
                        </div>
                        <div class="col-xs-2 top30">
                            <a href="<?php echo Util::caminho_projeto() ?>/orcamento/?action=del&id=<?php echo $i; ?>&tipo=produto" data-toggle="tooltip" data-placement="top" title="Excluir"> <i class="glyphicon glyphicon-remove"></i> </a>
                        </div>
                    </div>
                    <?php  
                }
            }
            ?>

            <div class="text-right bottom20">
                <a href="<?php echo Util::caminho_projeto() ?>/orcamento" title="Finalizar" class="btn btn-laranja" >
                    FINALIZAR
                </a>
            </div>


          </div>

        </div>
    </div>
  </div>
</div>



<div class="container top10">
  <div class="row col-xs-12 bg-menu">
    <div class="col-xs-2 col-xs-offset-1 fundo-logo">
      <a href="<?php echo Util::caminho_projeto() ?>">
        <img src="<?php echo Util::caminho_projeto() ?>/imgs/logo.png">
      </a>
    </div>
    <div class="col-xs-9">
        <!-- menu -->
         <nav class="navbar navbar-default navbar-right" role="navigation">
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-ex1-collapse top30">
               <ul class="nav navbar-nav">
                  <li class="active">
                     <a href="<?php echo Util::caminho_projeto() ?>">
                        HOME
                     </a>
                  </li>
                  <li >
                     <a href="<?php echo Util::caminho_projeto() ?>/empresa">
                        A EMPRESA
                     </a>
                  </li>
                  <li >
                     <a href="<?php echo Util::caminho_projeto() ?>/servicos">
                        SERVIÇOS
                     </a>
                  </li>
                  <li>
                     <a href="<?php echo Util::caminho_projeto() ?>/produtos">
                        PRODUTOS
                     </a>
                  </li>
                  <!-- <li>
                     <a href="<?php echo Util::caminho_projeto() ?>/portfolios">
                        PORTFÓLIO
                     </a>
                  </li> -->
                  <li>
                     <a href="<?php echo Util::caminho_projeto() ?>/especificacoes">
                        ESPECIFICAÇÕES
                     </a>
                  </li>
                  <li>
                     <a href="<?php echo Util::caminho_projeto() ?>/contato">
                        CONTATO
                     </a>
                  </li>
               </ul>
            </div>
            <!-- /.navbar-collapse -->
         </nav>
      <!-- menu -->
    </div>
  </div>
</div>