<?php  


// INTERNA
$url = Url::getURL(1);


if(!empty($url))
{
   $complemento = "AND url_amigavel = '$url'";
}

$result = $obj_site->select("tb_produtos", $complemento);

if(mysql_num_rows($result)==0)
{
  Util::script_location(Util::caminho_projeto()."/produtos");
}

$dados_dentro = mysql_fetch_array($result);
// BUSCA META TAGS E TITLE
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];


?>


<!DOCTYPE html>
<html lang="pt-br">
<head>
  <?php require_once('./includes/head.php'); ?>

  
  <script>
        $(window).load(function() {
          // The slider being synced must be initialized first
          $('#carousel').flexslider({
            animation: "slide",
            controlNav: false,
            animationLoop: false,
            slideshow: false,
            itemWidth: 70,
            itemMargin: 5,
            asNavFor: '#slider'
          });
         
          $('#slider').flexslider({
            animation: "slide",
            controlNav: false,
            animationLoop: false,
            slideshow: false,
            sync: "#carousel"
          });
        });
    </script>


</head>
<body class="bg-produtos">

  <!-- topo -->
  <?php require_once('./includes/topo.php') ?>
  <!-- topo -->


  <div class="container">
    <div class="row">
      <div class="col-xs-7 top25 descricao-banner-central">
            <h1 class="titulo-internas">NOSSOS PRODUTOS</h1>
            <?php $dados= $obj_site->select_unico("tb_empresa", "idempresa", 6) ?>
            <p><?php Util::imprime($dados[descricao]) ?></p>
        </div>
    </div>
  </div>  


  <!-- bg-empresa-->
    <div class="container-fluid container-barra top20">
        <div class="row">
            <div class="bg-lateral-barra"></div>
            <div class="container">
                <div class="row">
                    <div class="col-xs-5 bg-barra-lateral-interna">
                        <ol class="breadcrumb">
                          <li><a href="<?php echo Util::caminho_projeto() ?>">Home</a></li>
                          <li><a href="<?php echo Util::caminho_projeto() ?>/produtos">Produtos</a></li>
                          <li class="active"><?php Util::imprime($dados_dentro[titulo]) ?></li>
                      </ol>
                  </div>
              </div>
          </div>
      </div>
  </div>
  <!-- bg-servicos-->

<!--  BANNERS   -->
<!--  ==============================================================  -->
<!-- produtos dentro -->

<!-- solicite seu orçamento -->
<div class="container-fluid top60">
  <div class="row">
    <div class="container">
      <div class="row">
        <div class="col-xs-6 bottom40 produtos-dentro">
          <div class="">
            <div class="col-xs-8 left85 top130">
              <h1><?php Util::imprime($dados_dentro[titulo]) ?></h1>
            </div>
            <div class="col-xs-11 text-right  right20">
              <a href="javascript:void(0);" onclick="add_solicitacao(<?php Util::imprime($dados_dentro[0]) ?>, 'produto')" id="btn_add_solicitacao_<?php Util::imprime($dados_dentro[0]) ?>, 'produto'">
                <img src="<?php echo Util::caminho_projeto() ?>/imgs/solicite-orcamento.png" alt="">
              </a>
            </div>
          </div>
        </div>


        <div class="col-xs-5 col-xs-offset-1 top30">
          

          
          <div id="slider" class="flexslider">
            <ul class="slides slider-prod">
               <?php
                $result = $obj_site->select("tb_galerias_produtos", "AND id_produto = '$dados_dentro[0]' ");
                if(mysql_num_rows($result) > 0)
                {
                  while($row = mysql_fetch_array($result)){
                  ?>
                      <li>
                        <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 363, 309, array('class'=>'')); ?>
                      </li>
                  <?php 
                  }
                }
                ?>
              <!-- items mirrored twice, total of 12 -->
            </ul>
          </div>
          


        </div>


      </div>
    </div>
  </div>
</div>
<!-- solicite seu orçamento -->
<div class="container-fluir fundo-cinza001">
  <div class="row">
    <div class="container">
      <div class="row top20 bottom80">
        <div class="col-xs-6 ">
          <div class="descricao-orcamentos">
            <p><?php Util::imprime($dados_dentro[descricao]) ?></p>
         </div>
       </div>
       
       <div class="col-xs-3 col-xs-offset-2 descricao-orcamentos">
        
          <div id="carousel" class="flexslider slider-menor">
            <ul class="slides slider-prod-tumb">
              <?php
                $result = $obj_site->select("tb_galerias_produtos", "AND id_produto = '$dados_dentro[0]' ");
                if(mysql_num_rows($result) > 0)
                {
                  while($row = mysql_fetch_array($result)){
                  ?>
                      <li>
                        <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 50, 50, array('class'=>'img-circle')); ?>
                      </li>
                  <?php 
                  }
                }
                ?>
              <!-- items mirrored twice, total of 12 -->
            </ul>
          </div>
       </div>

      <div class="col-xs-12 top30">
        <div class="quem-somos1 bottom30">
          <h1>VEJA TAMBÉM:</h1>
          <img src="<?php echo Util::caminho_projeto() ?>/imgs/barra-quem-somos2.png" alt="">
        </div>
        

        <?php
        $result = $obj_site->select("tb_produtos", "ORDER By rand() LIMIT 4");
              if(mysql_num_rows($result) == 0){
                echo "<h2 class='bg-danger' style='padding: 20px;'>Nenhum produto encontrado.</h2>";
              }else{
                  while($row = mysql_fetch_array($result)){
                  ?>
                  <div class="col-xs-3">
                      <div class="lista-produtos">
                        <div class="thumbnail">
                          <a href="<?php echo Util::caminho_projeto() ?>/produto/<?php Util::imprime($row[url_amigavel]) ?>">
                            <img src="<?php echo Util::caminho_projeto() ?>/uploads/tumb_<?php Util::imprime($row[imagem]) ?>" alt="">
                          </a>
                          <div class="caption">
                            <h1><?php Util::imprime($row[titulo]) ?></h1>
                            <p><?php Util::imprime($row[descricao], 200) ?></p>
                           <p class="text-center">
                            <a href="<?php echo Util::caminho_projeto() ?>/produto/<?php Util::imprime($row[url_amigavel]) ?>" class="btn btn-primary text-center" role="button">
                              SAIBA MAIS
                            </a>
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                  <?php 
                  }
              }
              ?>


       

  

</div>


</div>
</div>
</div>
</div>

<!-- produtos dentro -->


<!--  ==============================================================  -->

<!--  ==============================================================  -->

<!-- rodape -->
<?php require_once('./includes/rodape.php') ?>
<!-- rodape -->

</html>


