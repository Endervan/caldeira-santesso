<?php  


// INTERNA
$url = Url::getURL(1);


if(!empty($url))
{
   $complemento = "AND url_amigavel = '$url'";
}

$result = $obj_site->select("tb_servicos", $complemento);

if(mysql_num_rows($result)==0)
{
  Util::script_location(Util::caminho_projeto()."/servicos");
}

$dados_dentro = mysql_fetch_array($result);
// BUSCA META TAGS E TITLE
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];


?>


<!DOCTYPE html>
<html lang="pt-br">
<head>
  <?php require_once('./includes/head.php'); ?>    


    <script>
        $(window).load(function() {
          // The slider being synced must be initialized first
          $('#carousel').flexslider({
            animation: "slide",
            controlNav: false,
            animationLoop: false,
            slideshow: false,
            itemWidth: 170,
            itemMargin: 5,
            asNavFor: '#slider'
          });
         
          $('#slider').flexslider({
            animation: "slide",
            controlNav: false,
            animationLoop: false,
            slideshow: false,
            sync: "#carousel"
          });
        });
    </script>


</head>
<body class="bg-servicos">

  <!-- topo -->
  <?php require_once('./includes/topo.php') ?>
  <!-- topo -->


  <div class="container">
    <div class="row">
      <div class="col-xs-7 top25 descricao-banner-central">
            <h1 class="titulo-internas">NOSSOS SERVIÇOS</h1>
            <?php $dados= $obj_site->select_unico("tb_empresa", "idempresa", 5) ?>
            <p><?php Util::imprime($dados[descricao]) ?></p>
        </div>
    </div>
  </div>  



  <!-- bg-empresa-->
    <div class="container-fluid container-barra top20">
        <div class="row">
            <div class="bg-lateral-barra"></div>
            <div class="container">
                <div class="row">
                    <div class="col-xs-4 bg-barra-lateral-interna">
                        <ol class="breadcrumb">
                          <li><a href="<?php echo Util::caminho_projeto() ?>">Home</a></li>
                          <li><a href="<?php echo Util::caminho_projeto() ?>/servicos">Servicos</a></li>
                          <li class="active"><?php Util::imprime($dados_dentro[titulo]) ?></li>
                      </ol>
                  </div>
              </div>
          </div>
      </div>
  </div>
  <!-- bg-servicos-->



  
  <!-- descricao -->
  <div class="container-fluid bg-branco top175 desc-serv">
    <div class="row">


      <div class="container desc-servico-dentro">
        <div class="row">

          <div class="col-xs-6">
              <h1><?php Util::imprime($dados_dentro[titulo]) ?></h1>
              <p><?php Util::imprime($dados_dentro[descricao]) ?></p>
          </div>

          <div class="col-xs-6 text-right img-serv-dentro">
              <?php $obj_site->redimensiona_imagem("../uploads/$dados_dentro[imagem]", 415, 415, array('class'=>'img-circle img-servicos')); ?>
              <a class="img-serv-dentro-btn" href="javascript:void(0);" onclick="add_solicitacao(<?php Util::imprime($dados_dentro[0]) ?>, 'servico')" id="btn_add_solicitacao_<?php Util::imprime($dados_dentro[0]) ?>, 'servico'">
                <img src="<?php echo Util::caminho_projeto() ?>/imgs/btn-solicite-orcamento.png" alt="">
              </a>
          </div>

        </div>
      </div>
    </div>    
  </div>
  <!-- descricao -->







  <!-- servicos -->
  <div class="container-fluir fundo-cinza001 ">
    <div class="row">
      <div class="container top200">
        <div class="row">


          
          <?php  
          $result = $obj_site->select("tb_galerias_servicos", "AND id_servico = '$dados_dentro[0]' ");
          if (mysql_num_rows($result) > 0) {
          ?>
            <!-- CONFIRA NOSS PORTIFOLIO -->
            <div class="col-xs-9 col-xs-offset-2">
              <div class="descricao-quem-somos">
                <h1 >CONFIRA NOSSO PORTIFÓLIO</h1>
                <img src="<?php echo Util::caminho_projeto() ?>/imgs/barra-quem-somos3.png" alt="">
              </div>
            </div>

            <div class="col-xs-9 col-xs-offset-2 slider-servicos">
                
                <div id="slider" class="flexslider">
                  <ul class="slides ">
                     <?php
                      while($row = mysql_fetch_array($result)){
                        $imagens[] = $row;
                      ?>
                          <li>
                            <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 600, 400, array('class'=>'')); ?>
                          </li>
                      <?php 
                      }
                      ?>
                    <!-- items mirrored twice, total of 12 -->
                  </ul>
                </div>

            </div>

            <div class="col-xs-6 col-xs-offset-2 bottom30 descricao-orcamentos">

              <div id="carousel" class="flexslider slider-menor">
                  <ul class="slides slider-prod-tumb">
                    <?php
                      if(count($imagens) > 0){
                        foreach($imagens as $row){
                        ?>
                            <li>
                              <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 75, 75); ?>
                            </li>
                        <?php 
                        }
                      }
                      ?>
                    <!-- items mirrored twice, total of 12 -->
                  </ul>
                </div>

            </div>
            <!-- CONFIRA NOSS PORTIFOLIO -->
            <?php 
            }
          ?>


        </div>
      </div>
    </div>
  </div>
  <!-- servicos -->





  <!-- rodape -->
  <?php require_once('./includes/rodape.php') ?>
  <!-- rodape -->

</body>
</html>


