
<!DOCTYPE html>
<html lang="pt-br">
<head>
  <?php require_once('./includes/head.php'); ?>


</head>
<body class="bg-produtos">

  <!-- topo -->
  <?php require_once('./includes/topo.php') ?>
  <!-- topo -->

  <div class="container">
    <div class="row">
      <div class="col-xs-7 top25 descricao-banner-central">
            <h1 class="titulo-internas">NOSSOS PRODUTOS</h1>
            <?php $dados= $obj_site->select_unico("tb_empresa", "idempresa", 6) ?>
            <p><?php Util::imprime($dados[descricao]) ?></p>
        </div>
    </div>
  </div>  


  <!-- bg-empresa-->
    <div class="container-fluid container-barra top20">
        <div class="row">
            <div class="bg-lateral-barra"></div>
            <div class="container">
                <div class="row">
                    <div class="col-xs-4 bg-barra-lateral-interna">
                        <ol class="breadcrumb">
                          <li><a href="<?php echo Util::caminho_projeto() ?>">Home</a></li>
                          <li class="active">Produtos</li>
                      </ol>
                  </div>
              </div>
          </div>
      </div>
  </div>
  <!-- bg-servicos-->


<!--  BANNERS   -->
<!--  ==============================================================  -->
<!-- menu esquerdo -->
<div class="container-fluid fundo-cinza001 ">
  <div class="row">
    <div class="container top100 bottom50">
      <div class="row">
        <div class="col-xs-12">

          <div class="col-xs-3">
            <ul class="nav nav-pills nav-stacked lista-categorias">
              
              <?php 
              $qtd_produtos = mysql_num_rows($obj_site->select("tb_produtos"));
              ?>
              <li role="presentation" class="visited"><a href="<?php echo Util::caminho_projeto() ?>/produtos"><span></span>TODOS</a></li>

              <?php 
              $result = $obj_site->select("tb_categorias_produtos");
              if (mysql_num_rows($result) > 0) {
                while ($row = mysql_fetch_array($result)) {
                  $qtd_produtos = mysql_num_rows($obj_site->select("tb_produtos", "AND id_categoriaproduto = '$row[0]'"));
                ?>
                <li role="presentation"><a href="<?php echo Util::caminho_projeto() ?>/produtos/<?php Util::imprime($row[url_amigavel]) ?>"><span></span><?php Util::imprime($row[titulo]) ?></a></li>
                <?php 
                }
              }
              ?>
            </ul>
          </div>

          


          <!-- lista de produtos -->
          <div class="col-xs-9">
            
               <?php
               $url1 = Url::getURL(1);

              //  FILTRA AS CATEGORIAS
              if (isset( $url1 )) {
                  $id_categoria = $obj_site->get_id_url_amigavel("tb_categorias_produtos", "idcategoriaproduto", $url1);
                  $complemento .= "AND id_categoriaproduto = '$id_categoria' ";
              }

              //  FILTRA PELO TITULO
              if(isset($_POST[busca_produtos])):
                $complemento = "AND titulo LIKE '%$_POST[busca_produtos]%'";
              endif;


              $result = $obj_site->select("tb_produtos", $complemento);
              if(mysql_num_rows($result) == 0){
                echo "<h2 class='bg-danger' style='padding: 20px;'>Nenhum produto encontrado.</h2>";
              }else{
                  while($row = mysql_fetch_array($result)){
                  ?>
                  <div class="col-xs-4">
                      <div class="lista-produtos">
                        <div class="thumbnail">
                          <a href="<?php echo Util::caminho_projeto() ?>/produto/<?php Util::imprime($row[url_amigavel]) ?>">
                            <img src="<?php echo Util::caminho_projeto() ?>/uploads/tumb_<?php Util::imprime($row[imagem]) ?>" alt="">
                          </a>
                          <div class="caption">
                            <h1><?php Util::imprime($row[titulo]) ?></h1>
                            <p><?php Util::imprime($row[descricao], 200) ?></p>
                           <p class="text-center">
                            <a href="<?php echo Util::caminho_projeto() ?>/produto/<?php Util::imprime($row[url_amigavel]) ?>" class="btn btn-primary text-center" role="button">
                              SAIBA MAIS
                            </a>
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                  <?php 
                  }
              }
              ?>

    <!-- lista de produtos -->
          </div>

</div>

          
</div>
</div>
</div>
</div>


<!--  ==============================================================  -->

<!--  ==============================================================  -->

<!-- rodape -->
<?php require_once('./includes/rodape.php') ?>
<!-- rodape -->

</html>


