
<!DOCTYPE html>
<html lang="pt-br">
<head>
  <?php require_once('./includes/head.php'); ?>    





</head>
<body class="bg-servicos">

  <!-- topo -->
  <?php require_once('./includes/topo.php') ?>
  <!-- topo -->
    
  <div class="container">
    <div class="row">
      <div class="col-xs-7 top25 descricao-banner-central">
            <h1 class="titulo-internas">NOSSOS SERVIÇOS</h1>
            <?php $dados= $obj_site->select_unico("tb_empresa", "idempresa", 5) ?>
            <p><?php Util::imprime($dados[descricao]) ?></p>
        </div>
    </div>
  </div>  




  <!-- bg-empresa-->
    <div class="container-fluid container-barra top20">
        <div class="row">
            <div class="bg-lateral-barra"></div>
            <div class="container">
                <div class="row">
                    <div class="col-xs-4 bg-barra-lateral-interna">
                        <ol class="breadcrumb">
                          <li><a href="<?php echo Util::caminho_projeto() ?>">Home</a></li>
                          <li class="active">Servicos</li>
                      </ol>
                  </div>
              </div>
          </div>
      </div>
  </div>
  <!-- bg-servicos-->


  <!-- servicos -->
  <div class="container-fluir fundo-cinza001">
    <div class="row">
      <div class="container">
        <div class="row bottom80">

        

        <?php 
        $result = $obj_site->select("tb_servicos");
        if (mysql_num_rows($result) > 0) {
          while ($row = mysql_fetch_array($result)) {
            if(++$i == 1){
              $classe1 = 'col-xs-7 descricao-servicos cabin';
              $classe2 = 'col-xs-5 posicao-imagem pull-right text-center';
            }else{
              $classe1 = 'col-xs-7 col-xs-offset-5 descricao-servicos cabin';
              $classe2 = 'col-xs-5 posicao-imagem pull-left text-center';
              $i = 0;
            }

          ?>
          <!-- item01 -->
          <div class="col-xs-12 lista-produtos11 top120">
            <div class="<?php echo $classe1 ?>">
              <h1><?php Util::imprime($row[titulo]) ?></h1>
              <p><?php Util::imprime($row[descricao], 1000) ?></p>
              <p class="text-right">
                <a href="servico/<?php Util::imprime($row[url_amigavel]) ?>" class="btn btn-primary text-center" role="button">SAIBA MAIS</a>
              </p>
            </div>
            
          </div>
          <div class="<?php echo $classe2 ?>">
            <a href="servico/<?php Util::imprime($row[url_amigavel]) ?>">
              <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 310, 310, array('class'=>'img-circle img-servicos')); ?>
            </a>
          </div>
          <?php 
          }
        }
        ?>

        </div>
      </div>
    </div>
  </div>
  <!-- servicos -->





  <!-- rodape -->
  <?php require_once('./includes/rodape.php') ?>
  <!-- rodape -->

</body>
</html>


