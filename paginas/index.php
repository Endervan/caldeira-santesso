
<!DOCTYPE html>
<html lang="pt-br">
<head>
  <?php require_once('./includes/head.php'); ?>    



  <!--    ==============================================================  -->
  <!--    ROYAL SLIDER    -->
  <!--    ==============================================================  -->

  <!-- slider JS files -->
  <script  src="<?php echo Util::caminho_projeto() ?>/jquery/cc-royalslider-9.2.0/royalslider/jquery-1.8.0.min.js"></script>
  <link href="<?php echo Util::caminho_projeto() ?>/jquery/cc-royalslider-9.2.0/royalslider/royalslider.css" rel="stylesheet">
  <script src="<?php echo Util::caminho_projeto() ?>/jquery/cc-royalslider-9.2.0/royalslider/jquery.royalslider.min.js"></script>
  <script>
    jQuery(document).ready(function($) {
                // Please note that autoHeight option has some conflicts with options like imageScaleMode, imageAlignCenter and autoScaleSlider
                // it's recommended to disable them when using autoHeight module
                $('#content-slider-1').royalSlider({
                  autoHeight: true,
                  arrowsNav: true,
                  fadeinLoadedSlide: false,
                  controlNavigationSpacing: 0,
                  controlNavigation: 'bullets',
                  imageScaleMode: 'none',
                  imageAlignCenter: false,
                  loop: true,
                  loopRewind: true,
                  numImagesToPreload: 6,
                  keyboardNavEnabled: true,
                  usePreloader: false,
                  autoPlay: {
                        // autoplay options go gere
                        enabled: true,
                        pauseOnHover: true
                      }

                    });
              });
</script>


</head>
<body>




  <!-- ==============================================================  -->
  <!--  BANNERS   -->
  <div class="container-fluid">
    <div class="row">
      <div id="container_banner">
       <div id="content_slider">
        <div id="content-slider-1" class="contentSlider rsDefault">



         <!-- ITEM -->
         <?php 
         $result = $obj_site->select("tb_banners", "AND tipo_banner = 1 and imagem<> '' ORDER BY rand() LIMIT 5");
         if (mysql_num_rows($result) > 0) {
           while ($row = mysql_fetch_array($result)) {
            if ($row[url] != '') {
            ?>
              <div>
                <a href="<?php Util::imprime($row[url]) ?>">
                  <img class="rsImg" src="./uploads/<?php Util::imprime($row[imagem]) ?>"/>
                </a>
              </div>
            <?php
            }else{
            ?>
              <div>
                <img class="rsImg" src="./uploads/<?php Util::imprime($row[imagem]) ?>"/>
              </div>
            <?php
            }
           }
         }
         ?>
        <!-- FIM DO ITEM -->
      </div>
    </div>
  </div>
</div>
</div>    
<!--  BANNERS   -->
<!--  ==============================================================  -->
<!-- menu esquerdo -->
<div class="container-fluid">
  <div class="row menus">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <div class="col-xs-3">
            <ul class="nav nav-pills nav-stacked lista-categorias">
              
              <?php 
              $qtd_produtos = mysql_num_rows($obj_site->select("tb_produtos"));
              ?>
              <li role="presentation" class="visited"><a href="<?php echo Util::caminho_projeto() ?>/produtos"><span></span>TODOS</a></li>


              <?php 
              $result = $obj_site->select("tb_categorias_produtos");
              if (mysql_num_rows($result) > 0) {
                while ($row = mysql_fetch_array($result)) {
                  $qtd_produtos = mysql_num_rows($obj_site->select("tb_produtos", "AND id_categoriaproduto = '$row[0]'"));
                ?>
                <li role="presentation"><a href="<?php echo Util::caminho_projeto() ?>/produtos/<?php Util::imprime($row[url_amigavel]) ?>"><span></span><?php Util::imprime($row[titulo]) ?></a></li>
                <?php 
                }
              }
              ?>
            </ul>
          </div>

          <!-- lista de produtos -->
          <div class="col-xs-9">
               <?php
              $result = $obj_site->select("tb_produtos", "ORDER BY rand() LIMIT 6");
              if(mysql_num_rows($result) == 0){
                echo "<h2 class='bg-danger' style='padding: 20px;'>Nenhum produto encontrado.</h2>";
              }else{
                  while($row = mysql_fetch_array($result)){
                  ?>
                  <div class="col-xs-4">
                      <div class="lista-produtos">
                        <div class="thumbnail">
                          <a href="<?php echo Util::caminho_projeto() ?>/produto/<?php Util::imprime($row[url_amigavel]) ?>">
                            <img src="<?php echo Util::caminho_projeto() ?>/uploads/tumb_<?php Util::imprime($row[imagem]) ?>" alt="">
                          </a>
                          <div class="caption">
                            <h1><?php Util::imprime($row[titulo]) ?></h1>
                            <p><?php Util::imprime($row[descricao], 200) ?></p>
                           <p class="text-center">
                            <a href="<?php echo Util::caminho_projeto() ?>/produto/<?php Util::imprime($row[url_amigavel]) ?>" class="btn btn-primary text-center" role="button">
                              SAIBA MAIS
                            </a>
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                  <?php 
                  }
              }
              ?>
          </div>
          <!-- lista de produtos -->
</div>
</div>
</div>
</div>
</div>

<!-- descricao de servicos -->
<!-- acount vertical     -->
<div class="container-fluir servicos">
  <div class="row">
    <div class="container">
      <div class="col-xs-12">
        <ul class="accordion" id="accordion">

          <?php 
          $result = $obj_site->select("tb_servicos", "ORDER BY rand() LIMIT 6");
          if (mysql_num_rows($result) > 0) {
            while ($row = mysql_fetch_array($result)) {
            ?>
            <li class="" style="background-image: url(./uploads/slider_<?php Util::imprime($row[imagem]) ?>);">
              <div class="bgDescription "></div>
              <div class="description">
                <a href="<?php echo Util::caminho_projeto() ?>/servico/<?php Util::imprime($row[url_amigavel]) ?>"> 
                  <h2><?php Util::imprime($row[titulo]) ?></h2>
                  <p><?php Util::imprime($row[descricao], 200) ?></p>
                </a>
              </div>
            </li>
            <?php 
            }
          }
          ?>

        </ul>
      </div>
    </div>
  </div>
</div>
<!-- descricao de servicos -->




<!-- nossas dicas -->
<div class="container-fluir exemplo-dicas top20">
  <div class="row">
    <div class="container">
      <div class="col-xs-12 bottom15">
        <div class="quem-somos1">
          <h1>NOSSAS ESPECIFICAÇÕES</h1>
          <img src="./imgs/barra-quem-somos1.png" alt="">
        </div>
      </div>
      <!-- thumbais -->
      
      
      <?php 
      $result = $obj_site->select("tb_dicas", "ORDER BY rand() LIMIT 4");
      if (mysql_num_rows($result) > 0) {
        while ($row = mysql_fetch_array($result)) {
        ?>
        <div class="col-xs-3 ">
            <div class="">
              <div class="thumbnail lista-produtos1 ">
                <a href="<?php echo Util::caminho_projeto() ?>/especificacao/<?php Util::imprime($row[url_amigavel]) ?>">
                  <img src="<?php echo Util::caminho_projeto() ?>/uploads/tumb_<?php Util::imprime($row[imagem]) ?>" alt="">
                  <div class="caption">
                    <h1><?php Util::imprime($row[titulo]) ?></h1>
                    <p><?php Util::imprime($row[descricao], 200) ?></p>
                 </div>
               </a>
             </div>
           </div>
         </div>
        <?php  
        }
      }
      ?>
<!-- thumbais -->
</div>
</div>
</div>
<!-- nossas dicas -->

<!--  ==============================================================  -->
<!-- conteudo -->

<div class="pagina">
  <!-- topo -->
  <?php require_once('./includes/topo.php') ?>
  <!-- topo -->
</div>

<!-- conteudo -->
<!--  ==============================================================  -->

<!-- rodape -->
<?php require_once('./includes/rodape.php') ?>
<!-- rodape -->

</body>
</html>



<!-- The JavaScript -->
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<script type="text/javascript">
  $(function() {
    $('#accordion > li').hover(
      function () {
        var $this = $(this);
        $this.stop().animate({'width':'600px'},500);
        $('.heading',$this).stop(true,true).fadeOut();
        $('.bgDescription',$this).stop(true,true).slideDown(500);
        $('.description',$this).stop(true,true).fadeIn();
      },
      function () {
        var $this = $(this);
        $this.stop().animate({'width':'30px'},1000);
        $('.heading',$this).stop(true,true).fadeIn();
        $('.description',$this).stop(true,true).fadeOut(500);
        $('.bgDescription',$this).stop(true,true).slideUp(700);
      }
      );
  });
</script>

