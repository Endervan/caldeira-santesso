
<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <?php require_once('./includes/head.php'); ?>    




    
    </head>
      <body class="bg-portfolio">
        
        <!-- topo -->
        <?php require_once('./includes/topo.php') ?>
        <!-- topo -->


        <!-- titulo da pagina -->
        <div class="container top30">
            <div class="row titulo-pagina">
                <div class="col-xs-9 col-xs-offset-3">
                    <h1>ALGUNS SERVIÇOS REALIZADOS SAN REMO</h1>
                </div>
            </div>
        </div>
        <!-- titulo da pagina -->
        
      
            
        <!-- conteudo -->
        <div class="container top50 bg-branco">
        

            
            <!-- lista portifolio -->
            <div class="row top20">
                <?php 
                $result = $obj_site->select("tb_portifolios");
                if (mysql_num_rows($result) == 0) {
                    echo "<h2 class='bg-info' style='padding: 20px;'>Nenhum registro encontrado.</h2>";
                }else{
                    while ($row = mysql_fetch_array($result)) {
                    ?>
                    <div class="col-xs-6 lista-portifolio-dentro">
                        <a href="<?php echo Util::caminho_projeto() ?>/portfolio/<?php Util::imprime($row[url_amigavel]) ?>">
                            <img src="<?php echo Util::caminho_projeto() ?>/uploads/tumb_<?php Util::imprime($row[imagem]) ?>" class="input100" alt="">
                        </a>
                        <h1><?php Util::imprime($row[titulo]) ?></h1>
                        <p><?php Util::imprime($row[descricao], 200) ?></p>
                        <a href="<?php echo Util::caminho_projeto() ?>/portfolio/<?php Util::imprime($row[url_amigavel]) ?>" class="btn btn-default">SAIBA MAIS</a>
                    </div>
                    <?php    
                    }
                }
                ?>
            </div>
            <!-- lista portifolio -->


        </div>
        <!-- conteudo -->





        <!-- rodape -->
        <?php require_once('./includes/rodape.php') ?>
        <!-- rodape -->

    </body>
</html>
