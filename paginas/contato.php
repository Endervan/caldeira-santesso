
<!DOCTYPE html>
<html lang="pt-br">
<head>
  <?php require_once('./includes/head.php'); ?>    





</head>
<body class="bg-contatos">

  <!-- topo -->
  <?php require_once('./includes/topo.php') ?>
  <!-- topo -->

  
  <div class="container">
    <div class="row">
      <div class="col-xs-7 top25 descricao-banner-central">
            <h1 class="titulo-internas">ENTRE EM CONTATO</h1>
            <?php $dados= $obj_site->select_unico("tb_empresa", "idempresa", 1) ?>
            <p><?php //Util::imprime($dados[descricao]) ?></p>
        </div>
    </div>
  </div>  


  <!-- bg-contatos-->
    <div class="container-fluid container-barra">
        <div class="row">
            <div class="bg-lateral-barra"></div>
            <div class="container">
                <div class="row">
                    <div class="col-xs-4 bg-barra-lateral-interna">
                        <ol class="breadcrumb">
                          <li><a href="#">Home</a></li>
                          <li class="active">Contato</li>
                      </ol>
                  </div>
              </div>
          </div>
      </div>
  </div>
  <!-- bg-contatos-->



  <!-- contatos -->
  <div class="container ">
    <div class="row bottom20 ">
      <div class="col-xs-12 descer2 menu-empresa text-right">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs menu-contatos" role="tablist" style="margin-left: 470px;">
          <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">FALE CONOSCO</a></li>
          <?php /* ?><li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">TRABALHE CONOSCO</a></li><?php */ ?>
          <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">COMO CHEGAR</a></li>
        </ul>
      </div>
      <div class="col-xs-10 descricao-contatos">

        <div class="col-xs-8 col-xs-offset-3 bottom20">
          <div class="pull-left right20">
            <h1>TELEFONE(S):<span><?php Util::imprime($config[telefone1]) ?> <?php if($config[telefone2] != ''){ Util::imprime(' / ' .$config[telefone2]); }  ?></span></h1>    
          </div>

        </div>
        
        <div class="col-xs-12">
          <!-- Tab panes -->
          <div class="tab-content">

            <!-- fale conosco -->
            <div role="tabpanel" class="tab-pane fade in active col-xs-offset-1" id="home">
              <?php  
                            //  VERIFICO SE E PARA ENVIAR O EMAIL
              if(isset($_POST[btn_contato]))
              {
                $nome_remetente =($_POST[nome]);
                $email =($_POST[email]);
                $assunto =($_POST[assunto]);
                $telefone =($_POST[telefone]);
                $mensagem =(nl2br($_POST[mensagem]));
                $texto_mensagem = "
                                  Nome: $nome_remetente <br />
                                  Assunto: $assunto <br />
                                  Telefone: $telefone <br />
                                  Email: $email <br />
                                  Mensagem: <br />
                                  $mensagem
                                  ";
               Util::envia_email($config[email], utf8_decode($assunto), $texto_mensagem, utf8_decode($nome_remetente), $email);
                Util::envia_email($config[email_copia], utf8_decode($assunto), $texto_mensagem, utf8_decode($nome_remetente), $email);
                Util::alert_bootstrap("Obrigado por entrar em contato.");
                unset($_POST);
              }
              ?>

              <form class="form-inline FormContato" role="form" method="post">

                <div class="row">
                  <div class="col-xs-6 form-group ">
                    <label class="glyphicon glyphicon-user"> <span>Nome</span></label>
                    <input type="text" name="nome" class="form-control fundo-form input100" placeholder="">
                  </div>
                  <div class="col-xs-6 form-group">
                    <label class="glyphicon glyphicon-envelope"> <span>E-mail</span></label>
                    <input type="text" name="email" class="form-control fundo-form input100" placeholder="">
                  </div>

                  <div class="col-xs-6 top20 form-group">
                    <label class="glyphicon glyphicon-earphone"> <span>Telefone</span></label>
                    <input type="text" name="telefone" class="form-control fundo-form input100" placeholder="">
                  </div>
                  <div class="col-xs-6 top20 form-group">
                    <label class="glyphicon glyphicon-star"> <span>Assunto</span></label>
                    <input type="text" name="assunto" class="form-control fundo-form input100" placeholder="">
                  </div>

                  <div class="col-xs-12 top20 form-group">
                    <label class="glyphicon glyphicon-pencil"> <span>Sua Mensagem</span></label>
                    <textarea name="mensagem" id="" cols="30" rows="10" class="form-control  fundo-form input100" placeholder=""></textarea>
                  </div>

                  <div class="clearfix"></div>

                  <div class="text-right right15 top30">
                    <button type="submit" class="btn btn-laranja1" name="btn_contato">
                      ENVIAR
                    </button>
                  </div>
                </div>

              </form>
            </div>
            <!-- fale conosco -->



            <!-- trabalhe conosco -->
            <div role="tabpanel" class="tab-pane fade col-xs-offset-1" id="profile">
              <?php  
                              //  VERIFICO SE E PARA ENVIAR O EMAIL
              if(isset($_POST[btn_trabalhe_conosco]))
              {
                $nome_remetente = ($_POST[nome]);
                $assunto = ($_POST[assunto]);
                $email = ($_POST[email]);
                $telefone = ($_POST[telefone]);
                $escolaridade = ($_POST[escolaridade]);
                $cargo = ($_POST[cargo]);
                $area = ($_POST[area]);
                $mensagem = (nl2br($_POST[mensagem]));

                if(!empty($_FILES[curriculo][name])):
                  $nome_arquivo = Util::upload_arquivo("./uploads", $_FILES[curriculo]);
                $texto = "Anexo: ";
                $texto .= "Clique ou copie e cole o link abaixo no seu navegador de internet para visualizar o arquivo.<br>";
                $texto .= "<a href='".Util::caminho_projeto()."/uploads/$nome_arquivo' target='_blank'>".Util::caminho_projeto()."/uploads/$nome_arquivo</a>";
                endif;

                $texto_mensagem = "
                                  Nome: $nome_remetente <br />
                                  Assunto: $assunto <br />
                                  Telefone: $telefone <br />
                                  Email: $email <br />
                                  Escolaridade: $escolaridade <br />
                                  Cargo: $cargo <br />
                                  Área: $area <br />
                                  Mensagem: <br />
                                  $texto    <br><br>
                                  $mensagem
                                  ";


                Util::envia_email($config[email], "CURRÍCULO PELO SITE ".$_SERVER[SERVER_NAME], $texto_mensagem, utf8_decode($nome_remetente), $email);
                Util::envia_email($config[email_copia], "CURRÍCULO PELO SITE ".$_SERVER[SERVER_NAME],$texto_mensagem, utf8_decode($nome_remetente), $email);
                Util::alert_bootstrap("Obrigado por entrar em contato.");
                unset($_POST);
              }
              ?>

              <form class="form-inline FormCurriculo" role="form" method="post" enctype="multipart/form-data">
                <div class="row">
                  <div class="col-xs-6 form-group ">
                    <label class="glyphicon glyphicon-user"> <span>Nome</span></label>
                    <input type="text" name="nome" class="form-control fundo-form input100" placeholder="">
                  </div>
                  <div class="col-xs-6 form-group">
                    <label class="glyphicon glyphicon-envelope"> <span>E-mail</span></label>
                    <input type="text" name="email" class="form-control fundo-form input100" placeholder="">
                  </div>

                  <div class="col-xs-6 top20 form-group">
                    <label class="glyphicon glyphicon-earphone"> <span>Telefone</span></label>
                    <input type="text" name="telefone" class="form-control fundo-form input100" placeholder="">
                  </div>
                  <div class="col-xs-6 top20 form-group">
                    <label class="glyphicon glyphicon-star"> <span>Assunto</span></label>
                    <input type="text" name="assunto" class="form-control fundo-form input100" placeholder="">
                  </div>
                  <div class="col-xs-6 top20 form-group">
                    <label class="glyphicon glyphicon-file"> <span>Currículo</span></label>
                    <input type="file" name="curriculo" class="form-control fundo-form input100" placeholder="">
                  </div>




                  <div class="col-xs-6 top20 form-group">
                    <label class="glyphicon glyphicon-book"> <span>Escolaridade</span></label>
                    <input type="text" name="escolaridade" class="form-control fundo-form input100" placeholder="">
                  </div>

                  <div class="col-xs-6 top20 form-group">
                    <label class="glyphicon glyphicon-lock"> <span>Cargo</span></label>
                    <input type="text" name="cargo" class="form-control fundo-form input100" placeholder="">
                  </div>

                  <div class="col-xs-6 top20 form-group">
                    <label class="glyphicon glyphicon-briefcase"> <span>Area</span></label>
                    <input type="text" name="area" class="form-control fundo-form input100" placeholder="">
                  </div>




                  <div class="col-xs-12 top20 form-group">
                    <label class="glyphicon glyphicon-pencil"> <span>Sua Mensagem</span></label>
                    <textarea name="mensagem" id="" cols="30" rows="10" class="form-control  fundo-form input100" placeholder=""></textarea>
                  </div>
                  

                  <div class="clearfix"></div>

                  <div class="text-right right15 top30">
                    <button type="submit" class="btn btn-laranja1" name="btn_trabalhe_conosco">
                      ENVIAR
                    </button>
                  </div>
                </div>
              </form> 
            </div>
            <!-- trabalhe conosco -->



            <!-- como chegar -->
            <div role="tabpanel" class="tab-pane fade centralizar-mapa" id="messages">
              <iframe src="<?php Util::imprime($config[src_place]) ?>" width="100%" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
            <!-- como chegar -->


          </div>
          <!-- Tab panes -->
        </div>

      </div>
    </div>
  </div>
  <!-- contatos -->





  <!-- rodape -->
  <?php require_once('./includes/rodape.php') ?>
  <!-- rodape -->

</body>
</html>



<script>
  $(document).ready(function() {
    $('.FormContato').bootstrapValidator({
      message: 'This value is not valid',
      feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
       nome: {
        validators: {
          notEmpty: {

          }
        }
      },
      email: {
        validators: {
          notEmpty: {

          },
          emailAddress: {
            message: 'Esse endereço de email não é válido'
          }
        }
      },
      telefone: {
        validators: {
          notEmpty: {

          }
        }
      },
      assunto: {
        validators: {
          notEmpty: {

          }
        }
      },
      mensagem: {
        validators: {
          notEmpty: {

          }
        }
      }
    }
  });
  });
</script>











<script>
  $(document).ready(function() {
    $('.FormCurriculo').bootstrapValidator({
      message: 'This value is not valid',
      feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
       nome: {
        validators: {
          notEmpty: {

          }
        }
      },
      email: {
        validators: {
          notEmpty: {

          },
          emailAddress: {
            message: 'Esse endereço de email não é válido'
          }
        }
      },
      telefone: {
        validators: {
          notEmpty: {

          }
        }
      },
      assunto: {
        validators: {
          notEmpty: {

          }
        }
      },
      escolaridade: {
        validators: {
          notEmpty: {

          }
        }
      },
      curriculo: {
        validators: {
          notEmpty: {
            message: 'Por favor insira seu currículo'
          },
          file: {
            extension: 'doc,docx,pdf,rtf',
            type: 'application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/rtf',
                            maxSize: 5*1024*1024,   // 5 MB
                            message: 'O arquivo selecionado não é valido, ele deve ser (doc,docx,pdf,rtf) e 5 MB no máximo.'
                          }
                        }
                      },
                      mensagem: {
                        validators: {
                          notEmpty: {

                          }
                        }
                      }
                    }
                  });
});
</script>

