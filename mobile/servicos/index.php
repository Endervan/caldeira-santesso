<?php
require_once("../../class/Include.class.php"); 
$obj_site = new Site();
?>
<!doctype html>
<html>
<head>
  <?php require_once('../includes/head.php'); ?>

</head>


</head>


<body class="bg-servicos">


  <?php require_once('../includes/topo.php'); ?>

  <div class="container top90 sembg topdescription">
    <div class="row">
      <div class="col-xs-6 ">
        <?php $dados= $obj_site->select_unico("tb_empresa", "idempresa", 5) ?>
        <?php Util::imprime($dados[descricao], 200000) ?></p>   
      </div>
    </div>
  </div>

  
  <!-- localizacao -->
  <div class="container location">
    <div class="row">
      <div class="col-xs-12">
        <ol class="breadcrumb">
          <li><a href="<?php echo Util::caminho_projeto() ?>">Home</a></li>
          <li class="active">Serviço</li>
        </ol>
      </div>
    </div>
  </div>
  <!-- localizacao -->

  <!-- descricao-dicas -->
  <div class="container">
    <div class="row">


      <div class="col-xs-12">
        <select class="menu-produtos top5" id="menu-categorias">
          <option value="" selected=""></option> 
          <option value="<?php echo Util::caminho_projeto() ?>/mobile/produtos">TODAS AS CATEGORIAS</option>       
          <?php
          $result = $obj_site->select("tb_categorias_produtos", "ORDER BY rand() LIMIT 4");
          if(mysql_num_rows($result) > 0){
            while($row = mysql_fetch_array($result)){
              ?>
              <option value="<?php echo Util::caminho_projeto() ?>/mobile/produtos/?categoria=<?php Util::imprime($row[0]) ?>"><?php Util::imprime($row[titulo]) ?></option>
              <?php
            }
          }
          ?>
        </select>

      </div>
  </div>

</div>
<!-- descricao-dicas -->





<div class="container  top10">
  <div class="row">
  <?php
    
      $result = $obj_site->select("tb_servicos");
      if(mysql_num_rows($result) > 0){
        while($row = mysql_fetch_array($result)){
          ?>
          <div class="lista-servico col-xs-12 bg-branco top30">
            
              <a href="<?php echo Util::caminho_projeto() ?>/mobile/servicos/<?php Util::imprime($row[url_amigavel]) ?>" title="<?php Util::imprime($row[titulo]) ?>">
                <img src="<?php echo Util::caminho_projeto() ?>/uploads/tumb_<?php Util::imprime($row[imagem]) ?>" alt="" class="img-circle col-xs-4 img-servico">
              </a>
            
                <h1><?php Util::imprime($row[titulo]) ?></h1>
                <p><?php Util::imprime($row[descricao], 100) ?></p>
                <div class="text-right">
                  <a href="<?php echo Util::caminho_projeto() ?>/mobile/servicos/<?php Util::imprime($row[url_amigavel]) ?>" title="<?php Util::imprime($row[titulo]) ?>">
                    <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/botao-saiba-mais-home.png" alt="">
                  </a>
                </div>
            
          </div>
         

          
          <?php
        }
      }
      ?>
      </div>
</div>

<!-- dicas -->

<div class="container">
  <div class="row">
    <div class="bottom40"></div>
  </div>
</div>


<?php require_once('../includes/rodape.php'); ?>

</body>
</html>

<?php require_once("../includes/js_css.php"); ?>