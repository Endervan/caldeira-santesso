
<!-- topo telefones -->
<div class="container bg-topo top20 bottom10 btn-ligar">
  <div class="row">
    <div class="col-xs-6 text-right">
      <div class="topo-telefone">
        <?php if (!empty($config[telefone2])): ?>
          <h1><?php Util::imprime($config[telefone2]); ?>
              <a href="tel:+55<?php Util::imprime($config[telefone2]); ?>">
                <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/botao-chamar-topo.jpg" alt="">
            </a>
        </h1>
        <?php endif ?>
  </div>
</div>
<div class="col-xs-6 text-right">
  <div class="topo-telefone1">
    <?php if (!empty($config[telefone1])): ?>
          <h1><?php Util::imprime($config[telefone1]); ?>
              <a href="tel:+55<?php Util::imprime($config[telefone1]); ?>">
                <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/botao-chamar-topo.jpg" alt="">
            </a>
        </h1>
    <?php endif ?>
</div>
</div>

</div>
</div>
<!-- topo telefones -->


<!-- contatos -->
<div class="container bg-topo top5 bg-menu ">
    <div class="row">

        <div class="col-xs-12">
            <div class="logo-menu">
                <div class="col-xs-5 logo text-left">
                    <a href="<?php echo Util::caminho_projeto() ?>/mobile">
                        <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/logo.png"  alt="">
                    </a>
                </div>
                <div class="col-xs-7">
                    <div class="pull-right">
                        <div class="menu-contatos">

                            <select class="menu-home top15  " id="menu-site">
                                <option selected=""></option> 
                                <option value="<?php echo Util::caminho_projeto() ?>/mobile">HOME</option>       
                                <option value="<?php echo Util::caminho_projeto() ?>/mobile/empresa/">A EMPRESA</option>    
                                <option value="<?php echo Util::caminho_projeto() ?>/mobile/produtos/">PRODUTOS</option>     
                                <option value="<?php echo Util::caminho_projeto() ?>/mobile/especificacoes/">ESPECIFICAÇÕES</option>     
                                <option value="<?php echo Util::caminho_projeto() ?>/mobile/servicos/">SERVIÇOS</option>     
                                <option value="<?php echo Util::caminho_projeto() ?>/mobile/orcamentos/">ORÇAMENTO</option>     
                                <option value="<?php echo Util::caminho_projeto() ?>/mobile/contatos/">CONTATOS</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>
</div>     
<!-- contatos -->

<!-- sombra menu topo -->
<div class="container">
    <div class="row">
        <div class="sombra"></div>
    </div>
</div>
<!-- sombra menu topo -->






























