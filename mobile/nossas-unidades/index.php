<?php
require_once("../../class/Include.class.php"); 
$obj_site = new Site();
?>
<!doctype html>
<html>
<head>
  <?php require_once('../includes/head.php'); ?>


</head>


</head>

<body class="">


  <?php require_once('../includes/topo.php'); ?>


  <!-- BG-empresa -->
  <div class="container">
    <div class="row">
      <div class="bg-empresa">
        <div id="carousel-example-generic" class="carousel slide posicao-carroucel" data-ride="carousel">
          <!-- Wrapper for slides -->
          <div class="carousel-inner" role="listbox">
            <div class="item active">
              <img src="../imgs/bg-unidades.jpg"alt="...">
              <div class="carousel-caption">
              </div>
            </div>
          </div>
        </div>
      </div>  
      <!--BG- empresa -->
      
    </div>
  </div>
  
  <!-- dicas -->
  <div class="container">
    <div class="row">
      <div class="posicao-barra-transparente">
        <h5>VC ESTA EM:</h5>
        <ol class="breadcrumb padronizar-barra">
          <li><a href="http://localhost/clientes/hidrolife/mobile/dicas/dentro.php">Home</a></li>
          <li class="active">Unidades</li>
        </ol>  
      </div>
    </div>
  </div>



  <!-- lista produtos -->
  <div class="container">
    <div class="row top35">

      <div class="col-xs-12 tabs-contatos">


        <div>

          <!-- Nav tabs -->
          <ul class="nav nav-tabs navs-personalizados input100" role="tablist">
          <li role="presentation" class="active"><a href="#home" >FALE CONOSCO</a></li>
            <li role="presentation"><a href="../trabalhe-conosco/">TRABALHE CONOSCO</a></li>
            <li role="presentation"><a href="../nossas-unidades" >NOSSAS UNIDADES</a></li>
          </ul>

          <div class="col-xs-12 lista-contatos top10 ">
            <div class="pull-left  right10">
             <h3 class="top10"><img src="../imgs/icon-telefone.png" alt=""></label> UNIDADE BRASÍLIA:<span>(61) 3322-1025</span></h3>
           </div>
           <div class="  pull-right">
            <a href="tel:+55<?php Util::imprime($config[telefone1]) ?>" class="btn btn-azul">CHAMAR</a>
          </div>



        </div>

        <div class="col-xs-12 lista-contatos top10 bottom30">
          <div class="pull-left  right10">
           <h3 class="top10"><img src="../imgs/icon-telefone.png" alt=""></label> UNIDADE GOIÂNIA:<span>(61) 3322-1025</span></h3>
         </div>
         <div class="pull-right">
          <a href="tel:+55<?php Util::imprime($config[telefone1]) ?>" class="btn btn-azul">CHAMAR</a>
        </div>



      </div>





      <!-- Tab panes -->
      <div class="tab-content top30">

      



     

        <!-- como chegar -->
        <div role="tabpanel" class="tab-pane fade in active" id="messages">

          <div class="confira-servicos-home text-center ">
            <h4>UNIDADE BRASÍLIA</h4>
            <img src="../imgs/sombra-servicos-home.png" alt="">
          </div>

          <div class="col-xs-12 pbottom20 descricao-unidades">
            <img src="../imgs/icon-home.png" alt=""><span>QI LOTE 27/29 TAGUATINGA NORTE</span>
          </div>

          <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d15356.174360645788!2d-48.05868729999996!3d-15.801655400000005!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x935a336ac328ae1f%3A0xea5a3d2258cdd14f!2sTaguatinga+Norte%2C+Taguatinga%2C+DF!5e0!3m2!1spt-BR!2sbr!4v1441843366942" width="470" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>

          <div class="col-xs-6 col-xs-offset-2">
            <a href="">
              <p><a href="#" class="btn  btn-produtos1 bottom40 left20 text-right" role="button">
                <i class="fa fa-plus-square fa-2x left5"></i>
                ABRIR MAPA 
              </a></p> 
            </a>
          </div>

          <div class="confira-servicos-home text-center ">
            <h4>UNIDADE GOIÂNIA</h4>
            <img src="../imgs/sombra-servicos-home.png" alt="">
          </div>

          <div class="col-xs-12 pbottom20 descricao-unidades">
            <img src="../imgs/icon-home.png" alt=""><span>QI LOTE 27/29 TAGUATINGA NORTE</span>
          </div>

          <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d15356.174360645788!2d-48.05868729999996!3d-15.801655400000005!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x935a336ac328ae1f%3A0xea5a3d2258cdd14f!2sTaguatinga+Norte%2C+Taguatinga%2C+DF!5e0!3m2!1spt-BR!2sbr!4v1441843366942" width="470" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>

          <div class="col-xs-6 col-xs-offset-2">
            <a href="">
              <p><a href="#" class="btn  btn-produtos1 bottom40 left20 text-right" role="button">
                <i class="fa fa-plus-square fa-2x left5"></i>
                ABRIR MAPA 
              </a></p> 
            </a>
          </div>

        </div>
        <!-- como chegar -->


      </div>

    </div>



  </div>

</div>
</div>























<?php require_once('../includes/rodape.php'); ?>

</body>
</html>




<?php require_once("../includes/js_css.php"); ?>


<script>
  $(document).ready(function() {
    $('.FormContato').bootstrapValidator({
      message: 'This value is not valid',
      feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
       nome: {
        validators: {
          notEmpty: {

          }
        }
      },
      email: {
        validators: {
          notEmpty: {

          },
          emailAddress: {
            message: 'Esse endereço de email não é válido'
          }
        }
      },
      telefone: {
        validators: {
          notEmpty: {

          }
        }
      },
      assunto: {
        validators: {
          notEmpty: {

          }
        }
      },
      tipo_pessoa: {
        validators: {
          notEmpty: {

          }
        }
      },
      cidade: {
        validators: {
          notEmpty: {

          }
        }
      },
      estado: {
        validators: {
          notEmpty: {

          }
        }
      },
      mensagem: {
        validators: {
          notEmpty: {

          }
        }
      }
    }
  });
});
</script>










<script>
  $(document).ready(function() {
    $('.FormCurriculo').bootstrapValidator({
      message: 'This value is not valid',
      feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
       nome: {
        validators: {
          notEmpty: {

          }
        }
      },
      email: {
        validators: {
          notEmpty: {

          },
          emailAddress: {
            message: 'Esse endereço de email não é válido'
          }
        }
      },
      telefone: {
        validators: {
          notEmpty: {

          }
        }
      },
      assunto: {
        validators: {
          notEmpty: {

          }
        }
      },
      curriculo: {
        validators: {
          notEmpty: {
            message: 'Por favor insira seu currículo'
          },
          file: {
            extension: 'doc,docx,pdf,rtf',
            type: 'application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/rtf',
                            maxSize: 5*1024*1024,   // 5 MB
                            message: 'O arquivo selecionado não é valido, ele deve ser (doc,docx,pdf,rtf) e 5 MB no máximo.'
                          }
                        }
                      },
                      mensagem: {
                        validators: {
                          notEmpty: {

                          }
                        }
                      }
                    }
                  });
});
</script>



