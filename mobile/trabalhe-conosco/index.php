<?php
require_once("../../class/Include.class.php"); 
$obj_site = new Site();
?>
<!doctype html>
<html>
<head>
  <?php require_once('../includes/head.php'); ?>


</head>


</head>

<body class="">


  <?php require_once('../includes/topo.php'); ?>


  <!-- BG-empresa -->
  <div class="container">
    <div class="row">
      <div class="bg-empresa">
        <div id="carousel-example-generic" class="carousel slide posicao-carroucel" data-ride="carousel">
          <!-- Wrapper for slides -->
          <div class="carousel-inner" role="listbox">
            <div class="item active">
              <img src="../imgs/bg-contatos.jpg"alt="...">
              <div class="carousel-caption">
              </div>
            </div>
          </div>
        </div>
      </div>  
      <!--BG- empresa -->
      
    </div>
  </div>
  
  <!-- dicas -->
  <div class="container">
    <div class="row">
      <div class="posicao-barra-transparente">
        <h5>VC ESTA EM:</h5>
        <ol class="breadcrumb padronizar-barra">
          <li><a href="http://localhost/clientes/hidrolife/mobile/dicas/dentro.php">Home</a></li>
          <li class="active">Trabalhe-Conosco</li>
        </ol>  
      </div>
    </div>
  </div>



  <!-- lista produtos -->
  <div class="container">
    <div class="row top35">

      <div class="col-xs-12 tabs-contatos">


        <div>

          <!-- Nav tabs -->
          <ul class="nav nav-tabs navs-personalizados input100" role="tablist">
          <li role="presentation" ><a href="../contatos/" >FALE CONOSCO</a></li>
            <li role="presentation" class="active"><a href="#profile" >TRABALHE CONOSCO</a></li>
            <li role="presentation"><a href="../nossas-unidades" >NOSSAS UNIDADES</a></li>
          </ul>

          <div class="col-xs-12 lista-contatos top10 ">
            <div class="pull-left  right10">
             <h3 class="top10"><img src="../imgs/icon-telefone.png" alt=""></label> UNIDADE BRASÍLIA:<span>(61) 3322-1025</span></h3>
           </div>
           <div class="  pull-right">
            <a href="tel:+55<?php Util::imprime($config[telefone1]) ?>" class="btn btn-azul">CHAMAR</a>
          </div>



        </div>

        <div class="col-xs-12 lista-contatos top10 bottom30">
          <div class="pull-left  right10">
           <h3 class="top10"><img src="../imgs/icon-telefone.png" alt=""></label> UNIDADE GOIÂNIA:<span>(61) 3322-1025</span></h3>
         </div>
         <div class="pull-right">
          <a href="tel:+55<?php Util::imprime($config[telefone1]) ?>" class="btn btn-azul">CHAMAR</a>
        </div>



      </div>





      <!-- Tab panes -->
      <div class="tab-content top30">

    


        <!-- trabalhe conosco -->
        <div role="tabpanel" class="tab-pane fade in active" id="profile">
          <?php  
                      //  VERIFICO SE E PARA ENVIAR O EMAIL
          if(isset($_POST[btn_trabalhe_conosco]))
          {
            $nome_remetente = Util::trata_dados_formulario($_POST[nome]);
            $assunto = Util::trata_dados_formulario($_POST[assunto]);
            $email = Util::trata_dados_formulario($_POST[email]);
            $telefone = Util::trata_dados_formulario($_POST[telefone]);
            $mensagem = Util::trata_dados_formulario(nl2br($_POST[mensagem]));

            if(!empty($_FILES[curriculo][name])):
              $nome_arquivo = Util::upload_arquivo("./uploads", $_FILES[curriculo]);
            $texto = "Anexo: ";
            $texto .= "Clique ou copie e cole o link abaixo no seu navegador de internet para visualizar o arquivo.<br>";
            $texto .= "<a href='".Util::caminho_projeto()."/uploads/$nome_arquivo' target='_blank'>".Util::caminho_projeto()."/uploads/$nome_arquivo</a>";
            endif;

            $texto_mensagem = "
            Nome: $nome_remetente <br />
            Assunto: $assunto <br />
            Telefone: $telefone <br />
            Email: $email <br />
            Mensagem: <br />
            $texto    <br><br>
            $mensagem
            ";


            Util::envia_email($config[email], "CURRÍCULO PELO SITE ".$_SERVER[SERVER_NAME], $texto_mensagem, $nome_remetente, $email);
            Util::envia_email($config[email_copia], "CURRÍCULO PELO SITE ".$_SERVER[SERVER_NAME], $texto_mensagem, $nome_remetente, $email);
            Util::alert_bootstrap("Obrigado por entrar em contato.");
            unset($_POST);
          }
          ?>

          <div class="col-xs-12 fundo-cinza bottom40">
            <form class="form-inline FormCurriculo" role="form" method="post" enctype="multipart/form-data">
              <div class="row">
                <div class="col-xs-6 top20 form-group">
                  <label class="glyphicon glyphicon-user"> <span>Nome</span></label>
                  <input type="text" name="nome" class="form-control input100" placeholder="">
                </div>
                <div class="col-xs-6 top20 form-group">
                  <label class="glyphicon glyphicon-envelope"> <span>E-mail</span></label>
                  <input type="text" name="email" class="form-control input100" placeholder="">
                </div>

                <div class="col-xs-6 top20 form-group">
                  <label class="glyphicon glyphicon-briefcase"> <span>Escolaridade</span></label>
                  <input type="text" name="email" class="form-control input100" placeholder="">
                </div>

                <div class="col-xs-6 top20 form-group">
                  <label class="glyphicon glyphicon-folder-close"> <span>cargo</span></label>
                  <input type="text" name="email" class="form-control input100" placeholder="">
                </div>


                <div class="col-xs-6 top20 form-group">
                  <label class="glyphicon glyphicon-earphone"> <span>Telefone</span></label>
                  <input type="text" name="telefone" class="form-control input100" placeholder="">
                </div>

                <div class="col-xs-6 top20 form-group">
                  <label class="glyphicon glyphicon-user"> <span>Tipo de pessoa</span></label>
                  <input type="text" name="assunto" class="form-control input100" placeholder="">
                </div>


                <div class="col-xs-6 top20 form-group has-feedback">
                  <label class="glyphicon glyphicon-map-marker"> <span>Estado</span></label>
                  <input type="text" name="estado" class="form-control input100" placeholder="" data-bv-field="estado"><i class="form-control-feedback" data-bv-icon-for="estado" style="display: none;"></i>
                  <small class="help-block" data-bv-validator="notEmpty" data-bv-for="estado" data-bv-result="NOT_VALIDATED" style="display: none;">Por favor insira um valor</small>
                </div>

                <div class="col-xs-6 top20 form-group has-feedback">
                  <label class="glyphicon glyphicon-map-marker"> <span>Cidade</span></label>
                  <input type="text" name="cidade" class="form-control input100" placeholder="" data-bv-field="cidade"><i class="form-control-feedback" data-bv-icon-for="cidade" style="display: none;"></i>
                  <small class="help-block" data-bv-validator="notEmpty" data-bv-for="cidade" data-bv-result="NOT_VALIDATED" style="display: none;">Por favor insira um valor</small>
                </div>

                <div class="col-xs-12 top20 form-group">
                  <label class="glyphicon glyphicon-file"> <span>Currículo</span></label>
                  <input type="file" name="curriculo" class="form-control input100" placeholder="">
                </div>

                <div class="col-xs-12 top20 form-group">
                  <label class="glyphicon glyphicon-pencil"> <span>Mensagem</span></label>
                  <textarea name="mensagem" id="" cols="30" rows="10" class="form-control input100"></textarea>
                </div>

                <div class="clearfix"></div>

                <div class=" col-xs-12 text-right bottom20 top30">
                  <button type="submit" class="btn btn-cinza-orcamentos" name="btn_contato">
                    ENVIAR
                  </button>
                </div>

              </div>
            </form> 
          </div>
        </div>
        <!-- trabalhe conosco -->


       
      </div>

    </div>



  </div>

</div>
</div>























<?php require_once('../includes/rodape.php'); ?>

</body>
</html>




<?php require_once("../includes/js_css.php"); ?>


<script>
  $(document).ready(function() {
    $('.FormContato').bootstrapValidator({
      message: 'This value is not valid',
      feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
       nome: {
        validators: {
          notEmpty: {

          }
        }
      },
      email: {
        validators: {
          notEmpty: {

          },
          emailAddress: {
            message: 'Esse endereço de email não é válido'
          }
        }
      },
      telefone: {
        validators: {
          notEmpty: {

          }
        }
      },
      assunto: {
        validators: {
          notEmpty: {

          }
        }
      },
      tipo_pessoa: {
        validators: {
          notEmpty: {

          }
        }
      },
      cidade: {
        validators: {
          notEmpty: {

          }
        }
      },
      estado: {
        validators: {
          notEmpty: {

          }
        }
      },
      mensagem: {
        validators: {
          notEmpty: {

          }
        }
      }
    }
  });
});
</script>










<script>
  $(document).ready(function() {
    $('.FormCurriculo').bootstrapValidator({
      message: 'This value is not valid',
      feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
       nome: {
        validators: {
          notEmpty: {

          }
        }
      },
      email: {
        validators: {
          notEmpty: {

          },
          emailAddress: {
            message: 'Esse endereço de email não é válido'
          }
        }
      },
      telefone: {
        validators: {
          notEmpty: {

          }
        }
      },
      assunto: {
        validators: {
          notEmpty: {

          }
        }
      },
      curriculo: {
        validators: {
          notEmpty: {
            message: 'Por favor insira seu currículo'
          },
          file: {
            extension: 'doc,docx,pdf,rtf',
            type: 'application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/rtf',
                            maxSize: 5*1024*1024,   // 5 MB
                            message: 'O arquivo selecionado não é valido, ele deve ser (doc,docx,pdf,rtf) e 5 MB no máximo.'
                          }
                        }
                      },
                      mensagem: {
                        validators: {
                          notEmpty: {

                          }
                        }
                      }
                    }
                  });
});
</script>



