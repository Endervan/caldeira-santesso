<?php
ob_start();
session_start();
require_once("../../class/Include.class.php");
$obj_site = new Site();


//  EXCLUI UM ITEM
if(isset($_GET[action]))
{
  //  SELECIONO O TIPO
  switch($_GET[tipo])
  {
    case "produto":
    $id = $_GET[id];
    unset($_SESSION[solicitacoes_produtos][$id]);
    sort($_SESSION[solicitacoes_produtos]);
    break;
    case "servico":
    $id = $_GET[id];
    unset($_SESSION[solicitacoes_servicos][$id]);
    sort($_SESSION[solicitacoes_servicos]);
    break;
  }

}

?>
<!doctype html>
<html>
<head>
	<?php require_once('../includes/head.php'); ?>

</head>


</head>

<body class="bg-orcamento">


	<?php require_once('../includes/topo.php'); ?>

  <form action="" method="post" name="form-orcamento" class="form-orcamento">

  <div class="container top90 sembg topdescription">
    <div class="row">
      <div class="col-xs-6 ">
        <?php $dados= $obj_site->select_unico("tb_empresa", "idempresa", 7) ?>
        <?php Util::imprime($dados[descricao], 200000) ?></p>   
      </div>
    </div>
  </div>

  
  <!-- localizacao -->
  <div class="container location" style="margin-top: 0px;">
    <div class="row">
      <div class="col-xs-12">
        <ol class="breadcrumb">
          <li><a href="<?php echo Util::caminho_projeto() ?>">Home</a></li>
          <li class="active">Orçamento</li>
        </ol>
      </div>
    </div>
  </div>
  <!-- localizacao -->





  <?php
          //  VERIFICO SE E PARA CADASTRAR A SOLICITACAO
  if(isset($_POST[nome]))
  {

  
              //  CADASTRO OS PRODUTOS SOLICITADOS
    for($i=0; $i < count($_POST[qtd]); $i++)
    {         
      $dados = $obj_site->select_unico("tb_produtos", "idproduto", $_POST[idproduto][$i]);

      $itens .= "
      <tr>
        <td><p>". $_POST[qtd][$i] ."</p></td>
        <td><p>". utf8_encode($dados[titulo]) ."</p></td>
      </tr>
      ";
    }



    //  CADASTRO OS SERVICOS SOLICITADOS
    for($i=0; $i < count($_POST[qtd_servico]); $i++)
    {         
        $dados = $obj_site->select_unico("tb_servicos", "idservico", $_POST[idservico][$i]);
        
        $itens_serv .= "
                    <tr>
                        <td><p>". $_POST[qtd_servico][$i] ."</p></td>
                        <td><p>". utf8_encode($dados[titulo]) ."</p></td>
                     </tr>
                    ";
    }


              //  ENVIANDO A MENSAGEM PARA O CLIENTE
               $texto_mensagem = "
              O seguinte cliente fez uma solicitação pelo site. <br />

              Tipo de pessoa: $_POST[tipo_pessoa] <br />
              Nome: $_POST[nome] <br />
              Email: $_POST[email] <br />
              Telefone: $_POST[telefone] <br />
              Cidade: $_POST[cidade] <br />
              Estado: $_POST[estado] <br />
              Mensagem: <br />
              ".nl2br($_POST[mensagem])." <br />



              <br />
              <h2> Produtos selecionados:</h2> <br />

              <table width='100%' border='0' cellpadding='5' cellspacing='5'>

                <tr>
                  <td><h4>QTD</h4></td>
                  <td><h4>ITEM</h4></td>
                </tr>

                $itens

              </table>



              <br />
              <h2> Serviços selecionados:</h2> <br />

              <table width='100%' border='0' cellpadding='5' cellspacing='5'>

                <tr>
                  <td><h4>QTD</h4></td>
                  <td><h4>ITEM</h4></td>
                </tr>

                $itens_serv

              </table>
              ";


  

            Util::envia_email($config[email],utf8_decode("ORÇAMENTO PELO SITE ").$_SERVER[SERVER_NAME], $texto_mensagem, utf8_decode($nome_remetente), $email);
            Util::envia_email($config[email_copia],utf8_decode("ORÇAMENTO PELO SITE ").$_SERVER[SERVER_NAME],$texto_mensagem, utf8_decode($nome_remetente), $email);
            unset($_SESSION[solicitacoes_produtos]);
            unset($_SESSION[solicitacoes_servicos]);
            Util::alert_bootstrap("Orçamento enviado com sucesso. Em breve entraremos em contato.");

          }
  ?>  






  <!-- descricao-orcamentos -->
  <div class="container pbottom40">
    <div class="row">
      <div class="col-xs-12 top15 bottom30 text-center">
        <div class="descricao-orcamentos">
          <h2>ITENS DO ORÇAMENTO</h2>
        </div>
      </div>

      



    <div class="col-xs-12">

      <!-- itens carrinho -->
      <?php
        if (count($_SESSION[solicitacoes_produtos]) > 0) {
          echo '<h1>Produtos</h1>';
          for($i=0; $i < count($_SESSION[solicitacoes_produtos]); $i++)
          {
            $row = $obj_site->select_unico("tb_produtos", "idproduto", $_SESSION[solicitacoes_produtos][$i]);
            ?>
            <div class="lista-orcamentos1">
              <div class="lista-itens-carrinho1">
                <div class="col-xs-3">
                  <img src="<?php echo Util::caminho_projeto() ?>/uploads/tumb_<?php Util::imprime($row[imagem]) ?>" class="input100" >
                </div>
                <div class="col-xs-6">
                  <h1 class="top30"><?php Util::imprime($row[titulo]) ?></h1>
                </div>
                <div class="col-xs-2 top15">
                  <input type="text" class="input-lista-prod-orcamentos" name="qtd[]" value="1" data-toggle="tooltip" data-placement="top" title="" data-original-title="Digite a quantidade desejada">
                  <input name="idproduto[]" type="hidden" value="<?php echo $row[0]; ?>"  />
                </div>

                <div class="col-xs-1 top25">
                  <a href="<?php echo Util::caminho_projeto() ?>/mobile/orcamentos/?action=del&amp;id=0&amp;tipo=produto" data-toggle="tooltip" data-placement="top" title="" data-original-title="Excluir"> <i class="glyphicon glyphicon-remove"></i> </a>
                </div>
              </div>
            </div>
            <?php 
          }
        }
        ?>

      <!-- itens carrinho -->



      <!-- botao continuar orçamento -->
      <div class="col-xs-12">
        <a href="<?php echo Util::caminho_projeto() ?>/mobile/produtos" title="Continuar orçando" class="btn btn-vermelho">
        </a> 
      </div>






      <?php
            if (count($_SESSION[solicitacoes_servicos]) > 0) {
              echo '<h1>Serviços</h1>';
              for($i=0; $i < count($_SESSION[solicitacoes_servicos]); $i++)
              {
                $row = $obj_site->select_unico("tb_servicos", "idservico", $_SESSION[solicitacoes_servicos][$i]);
                ?>
                <div class="lista-orcamentos1">
                  <div class="lista-itens-carrinho1">
                    <div class="col-xs-3">
                      <img src="<?php echo Util::caminho_projeto() ?>/uploads/tumb_<?php Util::imprime($row[imagem]) ?>" class="input100" >
                    </div>
                    <div class="col-xs-6">
                      <h1 class="top30"><?php Util::imprime($row[titulo]) ?></h1>
                    </div>
                    <div class="col-xs-2 top15">
                      <input type="text" class="input-lista-prod-orcamentos" name="qtd_servico[]" value="1" data-toggle="tooltip" data-placement="top" title="" data-original-title="Digite a quantidade desejada">
                      <input name="idservico[]" type="hidden" value="<?php echo $row[0]; ?>"  />
                    </div>

                    <div class="col-xs-1 top25">
                      <a href="<?php echo Util::caminho_projeto() ?>/mobile/orcamentos/?action=del&amp;id=0&amp;tipo=produto" data-toggle="tooltip" data-placement="top" title="" data-original-title="Excluir"> <i class="glyphicon glyphicon-remove"></i> </a>
                    </div>
                  </div>
                </div>
                <?php 
              }
            }
            ?>


            <!-- botao continuar orçamento -->
          <div class="col-xs-12">
            <a href="<?php echo Util::caminho_projeto() ?>/mobile/servicos" title="Continuar orçando" class="btn btn-vermelho">
            </a> 
          </div>

          </div>

    </div>
  </div>
  <!-- descricao-orcamentos -->


  <!-- contatos orcamentos -->
  <div class="container bg-topo bottom10 bg-form">
    <div class="row">
      

      <div class="col-xs-12">


        <div class="topo-telefone pull-right left20">
          <h1><?php Util::imprime($config[telefone1]) ?> 
            <a href="tel:+55<?php Util::imprime($config[telefone1]) ?> ">
              <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/botao-chamar-topo.jpg" alt="">
            </a>
          </h1>
        </div>


        <div class="topo-telefone pull-right">
          <h1><?php Util::imprime($config[telefone2]) ?> 
            <a href="tel:+55<?php Util::imprime($config[telefone2]) ?> ">
              <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/botao-chamar-topo.jpg" alt="">
            </a>
          </h1>
        </div>


      </div>




    </div>
    <!-- formulario -->
    <div class="top15">
      <div class="row">
        <div class="col-xs-6 form-group ">
          <label class="glyphicon glyphicon-user"> <span>Nome</span></label>
          <input type="text" name="nome" class="form-control fundo-form1 input100" placeholder="">
        </div>

        <div class="col-xs-6 form-group ">
          <label class="glyphicon glyphicon-envelope"> <span>E-mail</span></label>
          <input type="text" name="email" class="form-control fundo-form1 input100" placeholder="">
        </div>
      </div>


      <div class="row">
        <div class="col-xs-6 form-group top20">
          <label class="glyphicon glyphicon-earphone"> <span>Telefone</span></label>
          <input type="text" name="telefone" class="form-control fundo-form1 input100" placeholder="">
        </div>

        <div class="col-xs-6 form-group top20">
         <label class="glyphicon glyphicon-star"> <span>Tipo de pessoa</span></label>
         <input type="text" name="tipo_pessoa" class="form-control fundo-form1 input100" placeholder="">
       </div>
     </div>

     <div class="row">
      <div class="col-xs-6 form-group top20">
        <label <i class="fa fa-globe"></i> <span>Cidade</span></label>
        <input type="text" name="cidade" class="form-control fundo-form1 input100" placeholder="">
      </div>

      <div class="col-xs-6 form-group top20">
       <label <i class="fa fa-globe"></i> <span>Estado</span></label>
       <input type="text" name="estado" class="form-control fundo-form1 input100" placeholder="">
     </div>
   </div>


   <div class="row">
    <div class="col-xs-12 top20 form-group">
      <label class="glyphicon glyphicon-pencil"> <span>Sua Mensagem</span></label>
      <textarea name="mensagem" id="" cols="30" rows="8" class="form-control  fundo-form1 input100" placeholder=""></textarea>
    </div>
  </div>
  <div class="clearfix"></div>

  <div class="text-right top30">
    <button type="submit" class="btn btn-laranja1" name="btn_contato">
      ENVIAR
    </button>
  </div>


</div>
</div>
<!-- formulario -->


</div>

<!-- contatos orcamentos -->





</form>

<?php require_once('../includes/rodape.php'); ?>

</body>
</html>

<?php require_once("../includes/js_css.php"); ?>

<script>
  $(document).ready(function() {
    $('.form-orcamento').bootstrapValidator({
      message: 'This value is not valid',
      feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
       nome: {
        validators: {
          notEmpty: {

          }
        }
      },
      email: {
        validators: {
          notEmpty: {

          },
          emailAddress: {
            message: 'Esse endereço de email não é válido'
          }
        }
      },
      telefone: {
        validators: {
          notEmpty: {

          }
        }
      },
      assunto: {
        validators: {
          notEmpty: {

          }
        }
      },
      cidade: {
        validators: {
          notEmpty: {

          }
        }
      },
      tipo_pessoa: {
        validators: {
          notEmpty: {

          }
        }
      },
      estado: {
        validators: {
          notEmpty: {

          }
        }
      },
      mensagem: {
        validators: {
          notEmpty: {

          }
        }
      }
    }
  });
});
</script>