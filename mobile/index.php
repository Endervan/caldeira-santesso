<?php
require_once("../class/Include.class.php");
$obj_site = new Site();
?>
<!doctype html>
<html>

<head>
  <?php require_once('./includes/head.php'); ?>

  <!-- ---- LAYER SLIDER ---- -->
  <link rel="stylesheet" href="<?php echo Util::caminho_projeto() ?>/jquery/touchcarousel/touchcarousel.css"/>
  <link rel="stylesheet" href="<?php echo Util::caminho_projeto() ?>/jquery/touchcarousel/black-and-white-skin/black-and-white-skin.css" />
  <script src="<?php echo Util::caminho_projeto() ?>/jquery/touchcarousel/jquery.touchcarousel-1.2.min.js"></script>

  <script type="text/javascript">
    $(document).ready(function() {
      $("#carousel-gallery").touchCarousel({
        itemsPerPage: 1,
        scrollbar: true,
        scrollbarAutoHide: true,
        scrollbarTheme: "dark",
        pagingNav: false,
        snapToItems: true,
        scrollToLast: false,
        useWebkit3d: true,
        loopItems: true
      });
    });
  </script>
  <!-- XXXX LAYER SLIDER XXXX -->


</head>

<body>


  <?php require_once('./includes/topo.php'); ?>



  <!-- slider -->
  <div class="container">
    <div class="row slider-index telefone-topo1">
      <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators navs-personalizados">
          <?php
          $result = $obj_site->select("tb_banners", "AND tipo_banner = 2 ORDER BY rand() LIMIT 6");
          if (mysql_num_rows($result) > 0) {
            $i = 0;
            while($row = mysql_fetch_array($result)){

                            //  selecionador da classe active
              if($i == 0){
                $active = 'active';
              }else{
                $active = '';
              }

              ?>

              <li data-target="#carousel-example-generic" data-slide-to="<?php echo $i; ?>" class="<?php echo $active ?>"></li>

              <?php
              $imagens[] = $row;
              $i++;
            }
          }
          ?>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
          <?php
          $i = 0;
          if (count($imagens) > 0) {
            foreach($imagens as $imagem){

                            //  selecionador da classe active
              if($i == 0){
                $active = 'active';
              }else{
                $active = '';
              }

              ?>
              <div class="item <?php echo $active ?>">
                <?php
                if ($imagem[url] != '') {
                  ?>
                  <a href="<?php Util::imprime($imagem[url]) ?>">
                    <img src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($imagem[imagem]) ?>" alt="">
                  </a>
                  <?php
                }else{
                  ?>
                  <img src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($imagem[imagem]) ?>" alt="">
                  <?php
                }
                ?>

              </div>
              <?php
              $i++;
            }
          }
          ?>
        </div>

        <!-- Controls -->
        <a class="left carousel-control set-personalizadas" href="#carousel-example-generic" role="button" data-slide="prev">
          <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control set-personalizadas" href="#carousel-example-generic" role="button" data-slide="next">
          <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>

      </div>
    </div>
  </div>
  <!-- slider -->


  <!-- produtos home-->
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <select class="menu-produtos" id="menu-categorias">
          <option value="" selected=""></option> 
          <option value="<?php echo Util::caminho_projeto() ?>/mobile/produtos">TODAS AS CATEGORIAS</option>       
          <?php
          $result = $obj_site->select("tb_categorias_produtos");
          if(mysql_num_rows($result) > 0){
            while($row = mysql_fetch_array($result)){
              ?>
              <option value="<?php echo Util::caminho_projeto() ?>/mobile/produtos/?categoria=<?php Util::imprime($row[0]) ?>"><?php Util::imprime($row[titulo]) ?></option>
              <?php
            }
          }
          ?>
        </select>

      </div>


      <?php
      $result = $obj_site->select("tb_produtos", "ORDER BY rand() LIMIT 4");
      if(mysql_num_rows($result) > 0){
        while($row = mysql_fetch_array($result)){
          ?>
          <div class="col-xs-6 top15">
            <div class="thumbnail produtos-home">
              <a href="<?php echo Util::caminho_projeto() ?>/mobile/produtos/<?php Util::imprime($row[url_amigavel]) ?>" title="<?php Util::imprime($row[titulo]) ?>">
                <img src="<?php echo Util::caminho_projeto() ?>/uploads/tumb_<?php Util::imprime($row[imagem]) ?>" alt="" class="input100">
              </a>
              <div class="caption">
                <h1><?php Util::imprime($row[titulo]) ?></h1>
                <p><?php Util::imprime($row[descricao], 100) ?></p>
                <a href="<?php echo Util::caminho_projeto() ?>/mobile/produtos/<?php Util::imprime($row[url_amigavel]) ?>" title="<?php Util::imprime($row[titulo]) ?>">
                  <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/botao-saiba-mais-home.png" alt="">
                </a>
              </div>
            </div>
          </div>
          <?php
        }
      }
      ?>


    </div>
  </div>
  <!-- produtos home-->

  <!-- servicos home-->
  <div class="container pbottom20">
    <div class="row">
      <div class="col-xs-12">
        <div class="servicos-home fundo-preto">
          <h2>NOSSOS SERVIÇOS</h2>
        </div>
      </div>
    </div>

    <!-- acoordin vertical -->
    <div class=" panel-group" id="accordion" role="tablist" aria-multiselectable="true">


      <?php
      $i = 0;
      $result = $obj_site->select("tb_servicos", "ORDER BY rand() LIMIT 4");
      if(mysql_num_rows($result) > 0){
        while($row = mysql_fetch_array($result)){
          ?>
          <!-- item 001 -->
          <div class="panel panel-default ">
            <div class="panel-heading" role="tab" id="heading<?php echo $i ?>">
              <h4 class="panel-title decricao-accordin">
                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $i ?>" aria-expanded="true" aria-controls="collapse<?php echo $i ?>">
                  <?php Util::imprime($row[titulo]) ?>
                  <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/seta-menu-topo.png" alt="" class="pull-right">
                </a>
              </h4>
            </div>
            <div id="collapse<?php echo $i ?>" class="panel-collapse collapse <?php if($i == 0){ echo 'in'; } ?>" role="tabpanel" aria-labelledby="heading<?php echo $i ?>">
              <div class="panel-body">
                <a href="<?php echo Util::caminho_projeto() ?>/mobile/servicos/<?php Util::imprime($row[url_amigavel]) ?>">
                  <img src="<?php echo Util::caminho_projeto() ?>/uploads/tumb_<?php Util::imprime($row[imagem]) ?>" alt="" class="input100">
                </a>
              </div>
            </div>
          </div>
          <?php
          $i++;
        }
      }
      ?>


      

    </div>
    <!-- acoordin vertical -->
  </div>
  <!-- servicos home-->



 

  <!-- nossa dicas home -->
  <div class="container fundo-branco">
    <div class="row">
      <div class="col-xs-12 top10">
        <div class="descricao-dicas-home">
          <h2>NOSSAS ESPECIFICAÇÕES</h2>
        </div>
      </div>


      <?php
      $result = $obj_site->select("tb_dicas", "ORDER BY rand() LIMIT 2");
      if(mysql_num_rows($result) > 0){
        while($row = mysql_fetch_array($result)){
          ?>
          <!-- BANNER01 -->
          <div class="col-xs-6 top15">
            <div class="thumbnail dicas-home text-center">
              <a href="<?php echo Util::caminho_projeto() ?>/mobile/especificacoes/<?php Util::imprime($row[url_amigavel]) ?>">
                <img src="<?php echo Util::caminho_projeto() ?>/uploads/tumb_<?php Util::imprime($row[imagem]) ?>" alt="" class="input100">
                <div class="caption">
                  <h1> <?php Util::imprime($row[titulo]) ?></h1>
                  <p> <?php Util::imprime($row[descricao], 100) ?></p>
                </div>
              </a>
            </div>
          </div>
          <?php
        }
      }
      ?>
    </div>
  </div>
  <!-- nossa dicas home -->





  <?php require_once('./includes/rodape.php'); ?>


</body>

</html>


<?php require_once("./includes/js_css.php"); ?>