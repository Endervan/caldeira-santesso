<?php
ob_start();
session_start();
require_once("../../class/Include.class.php");
$obj_site = new Site();

?>
<!doctype html>
<html>
<head>
  <?php require_once('../includes/head.php'); ?>

</head>


</head>
<body class="bg-contato">


  <?php require_once('../includes/topo.php'); ?>

  <form action="" method="post" name="form-orcamento" class="form-orcamento">

  <div class="container top90 sembg topdescription">
    <div class="row">
      <div class="col-xs-6 ">
        <?php $dados= $obj_site->select_unico("tb_empresa", "idempresa", 7) ?>
        <?php Util::imprime($dados[descricao], 200000) ?></p>   
      </div>
    </div>
  </div>

  
  <!-- localizacao -->
  <div class="container location" style="margin-top: 0px;">
    <div class="row">
      <div class="col-xs-12">
        <ol class="breadcrumb">
          <li><a href="<?php echo Util::caminho_projeto() ?>">Home</a></li>
          <li class="active">Orçamento</li>
        </ol>
      </div>
    </div>
  </div>
  <!-- localizacao -->




  <!-- descricao-orcamentos -->
  <div class="container pbottom40">





    <!-- contatos orcamentos -->
    
      <div class="row bg-form form-contato">
        <div class="col-xs-12 ">
          <div class="">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs text-center menu-contatos" role="tablist">
              <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">FALE CONOSCO</a></li>
              <?php /* ?><li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">TRABALHE CONOSCO</a></li><?php */ ?>
              <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">COMO CHEGAR</a></li>
            </ul>


            <!-- Tab panes -->
        <div class="tab-content">

          <!-- fale conosco -->
          <div role="tabpanel" class="tab-pane fade in active" id="home">
            <?php
                            //  VERIFICO SE E PARA ENVIAR O EMAIL
            if(isset($_POST[btn_contato]))
            {
              $nome_remetente = ($_POST[nome]);
              $email = ($_POST[email]);
              $assunto = ($_POST[assunto]);
              $telefone = ($_POST[telefone]);
              $mensagem = (nl2br($_POST[mensagem]));
              $texto_mensagem = "
              Nome: $nome_remetente <br />
              Assunto: $assunto <br />
              Telefone: $telefone <br />
              Email: $email <br />
              Mensagem: <br />
              $mensagem
              ";
             Util::envia_email($config[email], utf8_decode($assunto), $texto_mensagem, utf8_decode($nome_remetente), $email);
              Util::envia_email($config[email_copia], utf8_decode($assunto), $texto_mensagem, utf8_decode($nome_remetente), $email);
              Util::alert_bootstrap("Obrigado por entrar em contato.");
              unset($_POST);
            }
            ?>


           


            

            <div class="col-xs-12 top10 bottom20">

               <?php if (!empty($config[telefone2])): ?>
                  <div class="topo-telefone pull-right left20">
                    <h1><?php Util::imprime($config[telefone2]) ?> 
                      <a href="tel:+55<?php Util::imprime($config[telefone2]) ?> ">
                        <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/botao-chamar-topo.jpg" alt="">
                      </a>
                    </h1>
                  </div>
              <?php endif ?>
              
              <?php if (!empty($config[telefone1])): ?>
                <div class="topo-telefone pull-right">
                <h1><?php Util::imprime($config[telefone1]) ?> 
                  <a href="tel:+55<?php Util::imprime($config[telefone1]) ?> ">
                    <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/botao-chamar-topo.jpg" alt="">
                  </a>
                </h1>
              </div>
            <?php endif ?>

              
            </div>


            <form class="form-inline FormContato top20 pbottom20" role="form" method="post">

              <div class="row">
                <div class="col-xs-6 form-group ">
                  <label class="glyphicon glyphicon-user"> <span>Nome</span></label>
                  <input type="text" name="nome" class="form-control fundo-form input100" placeholder="">
                </div>
                <div class="col-xs-6 form-group">
                  <label class="glyphicon glyphicon-envelope"> <span>E-mail</span></label>
                  <input type="text" name="email" class="form-control fundo-form input100" placeholder="">
                </div>
              </div>

              <div class="row">
                <div class="col-xs-6 top20 form-group">
                  <label class="glyphicon glyphicon-earphone"> <span>Telefone</span></label>
                  <input type="text" name="telefone" class="form-control fundo-form input100" placeholder="">
                </div>
                <div class="col-xs-6 top20 form-group">
                  <label class="glyphicon glyphicon-star"> <span>Assunto</span></label>
                  <input type="text" name="assunto" class="form-control fundo-form input100" placeholder="">
                </div>

              </div>

              <div class="row">
                <div class="col-xs-12 top20 form-group">
                  <label class="glyphicon glyphicon-pencil"> <span>Sua Mensagem</span></label>
                  <textarea name="mensagem" id="" cols="30" rows="10" class="form-control  fundo-form input100" placeholder=""></textarea>
                </div>

              </div>

              <div class="clearfix"></div>

              <div class="text-right right15 top30">
                <button type="submit" class="btn btn-cinza-contatos" name="btn_contato">
                  ENVIAR
                </button>
              </div>


            </form>

          </div>

          <!-- fale conosco -->



          <!-- trabalhe conosco -->
          <div role="tabpanel" class="tab-pane fade" id="profile">
            <?php
                              //  VERIFICO SE E PARA ENVIAR O EMAIL
            if(isset($_POST[btn_trabalhe_conosco]))
            {
              $nome_remetente = ($_POST[nome]);
              $assunto = ($_POST[assunto]);
              $email = ($_POST[email]);
              $telefone = ($_POST[telefone]);
              $escolaridade = ($_POST[escolaridade]);
              $cargo = ($_POST[cargo]);
              $area = ($_POST[area]);
              $mensagem = (nl2br($_POST[mensagem]));

              if(!empty($_FILES[curriculo][name])):
                $nome_arquivo = Util::upload_arquivo("./uploads", $_FILES[curriculo]);
              $texto = "Anexo: ";
              $texto .= "Clique ou copie e cole o link abaixo no seu navegador de internet para visualizar o arquivo.<br>";
              $texto .= "<a href='".Util::caminho_projeto()."/uploads/$nome_arquivo' target='_blank'>".Util::caminho_projeto()."/uploads/$nome_arquivo</a>";
              endif;

              $texto_mensagem = "
              Nome: $nome_remetente <br />
              Assunto: $assunto <br />
              Telefone: $telefone <br />
              Email: $email <br />
              Escolaridade: $escolaridade <br />
              Cargo: $cargo <br />
              Área: $area <br />
              Mensagem: <br />
              $texto    <br><br>
              $mensagem
              ";


              Util::envia_email($config[email], "CURRÍCULO PELO SITE ".$_SERVER[SERVER_NAME], $texto_mensagem, $nome_remetente, $email);
              Util::envia_email($config[email_copia], "CURRÍCULO PELO SITE ".$_SERVER[SERVER_NAME], $texto_mensagem, $nome_remetente, $email);
              Util::alert_bootstrap("Obrigado por entrar em contato.");
              unset($_POST);
            }
            ?>


            <div class="col-xs-12 top10 bottom20">
              <?php if (!empty($config[telefone2])): ?>
                  <div class="topo-telefone pull-right left20">
                    <h1><?php Util::imprime($config[telefone2]) ?> 
                      <a href="tel:+55<?php Util::imprime($config[telefone2]) ?> ">
                        <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/botao-chamar-topo.jpg" alt="">
                      </a>
                    </h1>
                  </div>
              <?php endif ?>
              
              <?php if (!empty($config[telefone1])): ?>
                <div class="topo-telefone pull-right">
                <h1><?php Util::imprime($config[telefone1]) ?> 
                  <a href="tel:+55<?php Util::imprime($config[telefone1]) ?> ">
                    <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/botao-chamar-topo.jpg" alt="">
                  </a>
                </h1>
              </div>
            <?php endif ?>
            </div>


            <form class="form-inline FormCurriculo top20 pbottom20" role="form" method="post" enctype="multipart/form-data">

              <div class="row">
                <div class="col-xs-6 form-group ">
                  <label class="glyphicon glyphicon-user"> <span>Nome</span></label>
                  <input type="text" name="nome" class="form-control fundo-form input100" placeholder="">
                </div>
                <div class="col-xs-6 form-group">
                  <label class="glyphicon glyphicon-envelope"> <span>E-mail</span></label>
                  <input type="text" name="email" class="form-control fundo-form input100" placeholder="">
                </div>
              </div>

              <div class="row">
                <div class="col-xs-6 top20 form-group">
                  <label class="glyphicon glyphicon-earphone"> <span>Telefone</span></label>
                  <input type="text" name="telefone" class="form-control fundo-form input100" placeholder="">
                </div>
                <div class="col-xs-6 top20 form-group">
                  <label class="glyphicon glyphicon-star"> <span>Assunto</span></label>
                  <input type="text" name="assunto" class="form-control fundo-form input100" placeholder="">
                </div>

              </div>

              <div class="row">
                <div class="col-xs-6 top20 form-group">
                  <label class="glyphicon glyphicon-file"> <span>Currículo</span></label>
                  <input type="file" name="curriculo" class="form-control fundo-form input100" placeholder="">
                </div>
                <div class="col-xs-6 top20 form-group">
                  <label class="glyphicon glyphicon-book"> <span>Escolaridade</span></label>
                  <input type="text" name="escolaridade" class="form-control fundo-form input100" placeholder="">
                </div>
              </div>


              <div class="row">
                <div class="col-xs-6 top20 form-group">
                  <label class="glyphicon glyphicon-lock"> <span>Cargo</span></label>
                  <input type="text" name="cargo" class="form-control fundo-form input100" placeholder="">
                </div>

                <div class="col-xs-6 top20 form-group">
                  <label class="glyphicon glyphicon-briefcase"> <span>Area</span></label>
                  <input type="text" name="area" class="form-control fundo-form input100" placeholder="">
                </div>
              </div>




              <div class="row">
                <div class="col-xs-12 top20 form-group">
                  <label class="glyphicon glyphicon-pencil"> <span>Sua Mensagem</span></label>
                  <textarea name="mensagem" id="" cols="30" rows="10" class="form-control  fundo-form input100" placeholder=""></textarea>
                </div>

              </div>

              <div class="clearfix"></div>

              <div class="text-right right15 top30">
                <button type="submit" class="btn btn-cinza-contatos" name="btn_trabalhe_conosco">
                  ENVIAR
                </button>
              </div>

            </form>
          </div>
          <!-- trabalhe conosco -->



          <!-- como chegar -->
          <div role="tabpanel" class="tab-pane fade" id="messages">

          <!-- mapa-geral -->
      <div class="mapa-contatos top10">

        <div class="col-xs-12 top10 bottom20">
              <?php if (!empty($config[telefone2])): ?>
                  <div class="topo-telefone pull-right left20">
                    <h1><?php Util::imprime($config[telefone2]) ?> 
                      <a href="tel:+55<?php Util::imprime($config[telefone2]) ?> ">
                        <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/botao-chamar-topo.jpg" alt="">
                      </a>
                    </h1>
                  </div>
              <?php endif ?>
              
              <?php if (!empty($config[telefone1])): ?>
                <div class="topo-telefone pull-right">
                <h1><?php Util::imprime($config[telefone1]) ?> 
                  <a href="tel:+55<?php Util::imprime($config[telefone1]) ?> ">
                    <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/botao-chamar-topo.jpg" alt="">
                  </a>
                </h1>
              </div>
            <?php endif ?>
            </div>


        <iframe src="<?php Util::imprime($config[src_place]) ?>" width="100%" height="400" frameborder="0" style="border:0" allowfullscreen></iframe>
     </div>
        <!-- mapa-geral -->

          </div>
          <!-- como chegar -->


        </div>
        <!-- Tab panes -->


            




          </div> 
        </div>
      </div>
    <!-- formulario -->


<!-- formulario -->


</div>

<!-- contatos orcamentos -->





</form>

<?php require_once('../includes/rodape.php'); ?>

</body>
</html>

<?php require_once("../includes/js_css.php"); ?>

<script>
  $(document).ready(function() {
    $('.form-orcamento').bootstrapValidator({
      message: 'This value is not valid',
      feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
       nome: {
        validators: {
          notEmpty: {

          }
        }
      },
      email: {
        validators: {
          notEmpty: {

          },
          emailAddress: {
            message: 'Esse endereço de email não é válido'
          }
        }
      },
      telefone: {
        validators: {
          notEmpty: {

          }
        }
      },
      assunto: {
        validators: {
          notEmpty: {

          }
        }
      },
      cidade: {
        validators: {
          notEmpty: {

          }
        }
      },
      tipo_pessoa: {
        validators: {
          notEmpty: {

          }
        }
      },
      estado: {
        validators: {
          notEmpty: {

          }
        }
      },
      mensagem: {
        validators: {
          notEmpty: {

          }
        }
      }
    }
  });
});
</script>