-- phpMyAdmin SQL Dump
-- version 4.3.8
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Tempo de geração: 19/10/2015 às 09:11
-- Versão do servidor: 5.5.38-35.2
-- Versão do PHP: 5.4.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Banco de dados: `masmidia_caldeira_santesso`
--

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_banners`
--

CREATE TABLE IF NOT EXISTS `tb_banners` (
  `idbanner` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ativo` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `tipo_banner` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `tb_banners`
--

INSERT INTO `tb_banners` (`idbanner`, `titulo`, `imagem`, `ativo`, `ordem`, `tipo_banner`, `url_amigavel`, `url`) VALUES
(34, 'Banner 1', '0409201509193770884577.jpg', 'SIM', NULL, '1', 'banner-1', ''),
(35, 'Banner 2', '0409201509196756699192.jpg', 'SIM', NULL, '1', 'banner-2', ''),
(36, 'Banner 3', '0409201509206739224737.jpg', 'SIM', NULL, '1', 'banner-3', '');

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_categorias_produtos`
--

CREATE TABLE IF NOT EXISTS `tb_categorias_produtos` (
  `idcategoriaproduto` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `desCategoria_Dica_Modelcricao` longtext COLLATE utf8_unicode_ci,
  `imagem` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'imagem_nao_disponivel.jpg',
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `classe` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=54 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `tb_categorias_produtos`
--

INSERT INTO `tb_categorias_produtos` (`idcategoriaproduto`, `titulo`, `desCategoria_Dica_Modelcricao`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `title_google`, `keywords_google`, `description_google`, `classe`) VALUES
(49, 'CALDEIRAS', NULL, 'imagem_nao_disponivel.jpg', 'SIM', NULL, 'caldeiras', NULL, NULL, NULL, NULL),
(50, 'VULCANIZADORAS', NULL, 'imagem_nao_disponivel.jpg', 'SIM', NULL, 'vulcanizadoras', NULL, NULL, NULL, NULL),
(51, 'FILTRO CAPTADOR', NULL, 'imagem_nao_disponivel.jpg', 'SIM', NULL, 'filtro-captador', NULL, NULL, NULL, NULL),
(52, 'EXALTORES', NULL, 'imagem_nao_disponivel.jpg', 'SIM', NULL, 'exaltores', NULL, NULL, NULL, NULL),
(53, 'FORNALHAS', NULL, 'imagem_nao_disponivel.jpg', 'SIM', NULL, 'fornalhas', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_clientes`
--

CREATE TABLE IF NOT EXISTS `tb_clientes` (
  `idcliente` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricao` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ativo` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `title_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_google` longtext COLLATE utf8_unicode_ci,
  `keywords_google` longtext COLLATE utf8_unicode_ci,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `tb_clientes`
--

INSERT INTO `tb_clientes` (`idcliente`, `titulo`, `descricao`, `ativo`, `ordem`, `title_google`, `description_google`, `keywords_google`, `url_amigavel`, `imagem`) VALUES
(9, 'Cliente 1', NULL, 'SIM', NULL, NULL, NULL, NULL, 'cliente-1', '0109201512155156772695..jpg'),
(10, 'Cliente 2', NULL, 'SIM', NULL, NULL, NULL, NULL, 'cliente-2', '0109201512159864208400.jpeg'),
(11, 'Cliente 3', NULL, 'SIM', NULL, NULL, NULL, NULL, 'cliente-3', '0109201512155784627596..jpg'),
(12, 'Cliente 4', NULL, 'SIM', NULL, NULL, NULL, NULL, 'cliente-4', '0109201512156984470312..gif'),
(13, 'Cliente 5', NULL, 'SIM', NULL, NULL, NULL, NULL, 'cliente-5', '0109201512162883705848..jpg');

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_configuracoes`
--

CREATE TABLE IF NOT EXISTS `tb_configuracoes` (
  `idconfiguracao` int(10) unsigned NOT NULL,
  `title_google` longtext NOT NULL,
  `description_google` longtext NOT NULL,
  `keywords_google` longtext NOT NULL,
  `ativo` varchar(3) NOT NULL DEFAULT 'SIM',
  `ordem` int(10) unsigned NOT NULL,
  `url_amigavel` varchar(255) NOT NULL,
  `endereco` varchar(255) NOT NULL,
  `telefone1` varchar(255) NOT NULL,
  `telefone2` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `src_place` varchar(255) NOT NULL,
  `horario_1` varchar(255) DEFAULT NULL,
  `horario_2` varchar(255) DEFAULT NULL,
  `email_copia` varchar(255) DEFAULT NULL,
  `telefone3` varchar(255) DEFAULT NULL,
  `telefone4` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `tb_configuracoes`
--

INSERT INTO `tb_configuracoes` (`idconfiguracao`, `title_google`, `description_google`, `keywords_google`, `ativo`, `ordem`, `url_amigavel`, `endereco`, `telefone1`, `telefone2`, `email`, `src_place`, `horario_1`, `horario_2`, `email_copia`, `telefone3`, `telefone4`) VALUES
(1, 'Caldeira Santesso', '', '', 'SIM', 0, '', 'Quadra 100 Lote 2 - Taguatinga Sul - DF', '(61)3452-0987', '(61)3452-1111', '', '', NULL, NULL, NULL, '(61)3426-0543', '(61)3452-1236');

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_depoimentos`
--

CREATE TABLE IF NOT EXISTS `tb_depoimentos` (
  `iddepoimento` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cargo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `depoimento` longtext COLLATE utf8_unicode_ci,
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `tb_depoimentos`
--

INSERT INTO `tb_depoimentos` (`iddepoimento`, `titulo`, `cargo`, `depoimento`, `ativo`, `ordem`, `url_amigavel`, `imagem`) VALUES
(6, 'Carlos Eduardo Batista', 'Diretor da Vale', '<p>\r\n	Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sapiente aperiam odio hic cum veniam quod tempore sunt perferendis ab at sequi unde facilis eius reiciendis asperiores error porro, quo, velit.e aperiam odio hic cum veniam quod tempore sunt perferendis ab at sequi unde facilis e aperiam odio hic cum veniam quod tempore sunt perferendis ab at sequi unde facilis</p>', 'SIM', NULL, 'carlos-eduardo-batista', '0109201512587108189053..jpg'),
(7, 'Maria Paula Assunção', 'Gerente de Vendas', '<p>\r\n	Gerente Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet</p>', 'SIM', NULL, 'maria-paula-assuncao', NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_dicas`
--

CREATE TABLE IF NOT EXISTS `tb_dicas` (
  `iddica` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci,
  `imagem` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'imagem_nao_disponivel.jpg',
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` date DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=50 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `tb_dicas`
--

INSERT INTO `tb_dicas` (`iddica`, `titulo`, `descricao`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `title_google`, `keywords_google`, `description_google`, `data`) VALUES
(42, 'Dica 1', '<p>\r\n	Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', '0209201506598045992155..jpg', 'SIM', NULL, 'dica-1', NULL, NULL, NULL, NULL),
(43, 'Dica 2', '<p>\r\n	Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', '0209201507044515009861..jpg', 'SIM', NULL, 'dica-2', NULL, NULL, NULL, NULL),
(44, 'Dica 3', '<p>\r\n	Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', '0209201507055247031247..jpg', 'SIM', NULL, 'dica-3', NULL, NULL, NULL, NULL),
(45, 'Dica 4', '<p>\r\n	4&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', '0209201507059505955771..jpg', 'SIM', NULL, 'dica-4', NULL, NULL, NULL, NULL),
(46, 'Dica 5', '<p>\r\n	5&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', '0209201507051499702127..jpg', 'SIM', NULL, 'dica-5', NULL, NULL, NULL, NULL),
(47, 'Dica 6', '<p>\r\n	6&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', '0209201507068127507207..jpg', 'SIM', NULL, 'dica-6', NULL, NULL, NULL, NULL),
(48, 'Dica 7', '<p>\r\n	7&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', '0209201507065624973852..jpg', 'SIM', NULL, 'dica-7', NULL, NULL, NULL, NULL),
(49, 'Dica 8', '<p>\r\n	8&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', '0209201507069412590447..jpg', 'SIM', NULL, 'dica-8', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_empresa`
--

CREATE TABLE IF NOT EXISTS `tb_empresa` (
  `idempresa` int(10) unsigned NOT NULL,
  `titulo` varchar(80) NOT NULL,
  `descricao` longtext NOT NULL,
  `ativo` varchar(3) NOT NULL DEFAULT 'SIM',
  `ordem` int(10) unsigned NOT NULL,
  `title_google` varchar(255) NOT NULL,
  `keywords_google` varchar(255) NOT NULL,
  `description_google` varchar(255) NOT NULL,
  `url_amigavel` varchar(255) NOT NULL,
  `colaboradores_especializados` int(11) DEFAULT NULL,
  `trabalhos_entregues` int(11) DEFAULT NULL,
  `trabalhos_entregues_este_mes` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `tb_empresa`
--

INSERT INTO `tb_empresa` (`idempresa`, `titulo`, `descricao`, `ativo`, `ordem`, `title_google`, `keywords_google`, `description_google`, `url_amigavel`, `colaboradores_especializados`, `trabalhos_entregues`, `trabalhos_entregues_este_mes`) VALUES
(1, 'Empresa - Legenda', 'Legenda Ipsum é simplesmente texto fictício da impressão e da in dústria tipográfica. psum é simplesmente texto fictício da impressão e da in Ipsum é simplesmente texto fictício da impressão e da in dústria tipográfica. psum é simplesmente texto fictício da impressão e da in ', 'SIM', 0, '', '', '', '', NULL, NULL, NULL),
(2, 'Empresa - Nossa Missão', 'Nossa missão Ipsum é simplesmente texto fictício da impressão e da in dústria tipográfica. psum é simplesmente texto fictício da impressão e da in Ipsum é simplesmente texto fictício da impressão e da in dústria tipográfica. psum é simplesmente texto fictício da impressão e da in ', 'SIM', 0, '', '', '', '', NULL, NULL, NULL),
(3, 'Empresa - Nossa Visão', 'Nossa visão Ipsum é simplesmente texto fictício da impressão e da in dústria tipográfica. psum é simplesmente texto fictício da impressão e da in Ipsum é simplesmente texto fictício da impressão e da in dústria tipográfica. psum é simplesmente texto fictício da impressão e da in ', 'SIM', 0, '', '', '', '', NULL, NULL, NULL),
(4, 'Empresa - Nossos Valores', 'Nossos valores Ipsum é simplesmente texto fictício da impressão e da in dústria tipográfica. psum é simplesmente texto fictício da impressão e da in Ipsum é simplesmente texto fictício da impressão e da in dústria tipográfica. psum é simplesmente texto fictício da impressão e da in ', 'SIM', 0, '', '', '', '', NULL, NULL, NULL),
(5, 'Serviços - Legenda', 'Legenda Serviço Ipsum é simplesmente texto fictício da impressão e da in dústria tipográfica. psum é simplesmente texto fictício da impressão e da in Ipsum é simplesmente texto fictício da impressão e da in dústria tipográfica. psum é simplesmente texto fictício da impressão e da in ', 'SIM', 0, '', '', '', '', NULL, NULL, NULL),
(6, 'Produtos - Legenda', 'Legenda Produtos Ipsum é simplesmente texto fictício da impressão e da in dústria tipográfica. psum é simplesmente texto fictício da impressão e da in Ipsum é simplesmente texto fictício da impressão e da in dústria tipográfica. psum é simplesmente texto fictício da impressão e da in ', 'SIM', 0, '', '', '', '', NULL, NULL, NULL),
(7, 'Orçamento - Legenda', 'Legenda Orçamento Ipsum é simplesmente texto fictício da impressão e da in dústria tipográfica. psum é simplesmente texto fictício da impressão e da in Ipsum é simplesmente texto fictício da impressão e da in dústria tipográfica. psum é simplesmente texto fictício da impressão e da in ', 'SIM', 0, '', '', '', '', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_galerias_produtos`
--

CREATE TABLE IF NOT EXISTS `tb_galerias_produtos` (
  `id_galeriaproduto` int(11) NOT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `id_produto` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=89 DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `tb_galerias_produtos`
--

INSERT INTO `tb_galerias_produtos` (`id_galeriaproduto`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `id_produto`) VALUES
(60, '0209201501121372180961.jpg', 'SIM', NULL, NULL, 271),
(61, '0209201501128186390747.jpg', 'SIM', NULL, NULL, 271),
(62, '0209201501123248779897.jpg', 'SIM', NULL, NULL, 271),
(63, '0209201501121930283812.jpg', 'SIM', NULL, NULL, 271),
(64, '0209201505044740059301.jpg', 'SIM', NULL, NULL, 272),
(65, '0209201505048511372502.jpg', 'SIM', NULL, NULL, 272),
(66, '0209201505044545205446.jpg', 'SIM', NULL, NULL, 272),
(67, '0209201505047191714242.jpg', 'SIM', NULL, NULL, 272),
(68, '0209201505046124317745.jpg', 'SIM', NULL, NULL, 272),
(69, '0209201505046285685838.jpg', 'SIM', NULL, NULL, 273),
(70, '0209201505042441342150.jpg', 'SIM', NULL, NULL, 273),
(71, '0209201505044091869845.jpg', 'SIM', NULL, NULL, 273),
(72, '0209201505048439378746.jpg', 'SIM', NULL, NULL, 274),
(73, '0209201505047987934822.jpg', 'SIM', NULL, NULL, 273),
(74, '0209201505046696295899.jpg', 'SIM', NULL, NULL, 274),
(75, '0209201505049913366039.jpg', 'SIM', NULL, NULL, 274),
(76, '0209201505049208998729.jpg', 'SIM', NULL, NULL, 273),
(77, '0209201505041691120531.jpg', 'SIM', NULL, NULL, 275),
(78, '0209201505045441662126.jpg', 'SIM', NULL, NULL, 274),
(79, '0209201505045521689645.jpg', 'SIM', NULL, NULL, 275),
(80, '0209201505049235196399.jpg', 'SIM', NULL, NULL, 275),
(81, '0209201505047611521941.jpg', 'SIM', NULL, NULL, 274),
(82, '0209201505043138349975.jpg', 'SIM', NULL, NULL, 275),
(83, '0209201505046189182057.jpg', 'SIM', NULL, NULL, 275),
(84, '0209201505049428321723.jpg', 'SIM', NULL, NULL, 276),
(85, '0209201505046103741766.jpg', 'SIM', NULL, NULL, 276),
(86, '0209201505048625760326.jpg', 'SIM', NULL, NULL, 276),
(87, '0209201505043349410187.jpg', 'SIM', NULL, NULL, 276),
(88, '0209201505047693930773.jpg', 'SIM', NULL, NULL, 276);

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_galerias_servicos`
--

CREATE TABLE IF NOT EXISTS `tb_galerias_servicos` (
  `idgaleriaservico` int(11) NOT NULL,
  `id_servico` int(11) DEFAULT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `tb_galerias_servicos`
--

INSERT INTO `tb_galerias_servicos` (`idgaleriaservico`, `id_servico`, `imagem`, `ativo`, `ordem`) VALUES
(1, 36, '0209201510368816911487.jpg', 'SIM', NULL),
(2, 36, '0209201510368443919374.jpeg', 'SIM', NULL),
(3, 36, '0209201510365348381847.jpg', 'SIM', NULL),
(4, 36, '0209201510367792222181.jpg', 'SIM', NULL),
(5, 36, '0209201510369929104033.jpg', 'SIM', NULL),
(6, 37, '0209201510378591811392.jpg', 'SIM', NULL),
(7, 37, '0209201510374092702898.jpeg', 'SIM', NULL),
(8, 37, '0209201510374217629743.jpg', 'SIM', NULL),
(9, 37, '0209201510371998167745.jpg', 'SIM', NULL),
(10, 37, '0209201510376213156084.jpg', 'SIM', NULL),
(11, 38, '0209201510371771991078.jpg', 'SIM', NULL),
(12, 38, '0209201510375567460267.jpeg', 'SIM', NULL),
(13, 38, '0209201510373327259189.jpg', 'SIM', NULL),
(14, 39, '0209201510377202819854.jpg', 'SIM', NULL),
(15, 39, '0209201510373317656338.jpeg', 'SIM', NULL),
(16, 39, '0209201510373725799442.jpg', 'SIM', NULL),
(17, 39, '0209201510377658288880.jpg', 'SIM', NULL),
(18, 38, '0209201510376385150117.jpg', 'SIM', NULL),
(19, 39, '0209201510373384791943.jpg', 'SIM', NULL),
(20, 38, '0209201510377415764157.jpg', 'SIM', NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_logins`
--

CREATE TABLE IF NOT EXISTS `tb_logins` (
  `idlogin` int(11) NOT NULL,
  `nome` varchar(45) DEFAULT NULL,
  `senha` varchar(45) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `id_grupologin` int(11) NOT NULL,
  `email` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Fazendo dump de dados para tabela `tb_logins`
--

INSERT INTO `tb_logins` (`idlogin`, `nome`, `senha`, `ativo`, `id_grupologin`, `email`) VALUES
(5, 'HomeWeb', 'e10adc3949ba59abbe56e057f20f883e', 'SIM', 0, 'atendimento.sites@homewebbrasil.com.br');

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_logs_logins`
--

CREATE TABLE IF NOT EXISTS `tb_logs_logins` (
  `idloglogin` int(11) NOT NULL,
  `operacao` longtext,
  `consulta_sql` longtext,
  `data` date DEFAULT NULL,
  `hora` time DEFAULT NULL,
  `id_login` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1617 DEFAULT CHARSET=utf8;

--
-- Fazendo dump de dados para tabela `tb_logs_logins`
--

INSERT INTO `tb_logs_logins` (`idloglogin`, `operacao`, `consulta_sql`, `data`, `hora`, `id_login`) VALUES
(1580, 'CADASTRO DO CLIENTE ', '', '2015-09-01', '00:15:30', 5),
(1581, 'CADASTRO DO CLIENTE ', '', '2015-09-01', '00:15:39', 5),
(1582, 'CADASTRO DO CLIENTE ', '', '2015-09-01', '00:15:45', 5),
(1583, 'CADASTRO DO CLIENTE ', '', '2015-09-01', '00:15:53', 5),
(1584, 'CADASTRO DO CLIENTE ', '', '2015-09-01', '00:16:02', 5),
(1585, 'CADASTRO DO CLIENTE ', '', '2015-09-01', '00:58:04', 5),
(1586, 'CADASTRO DO CLIENTE ', '', '2015-09-01', '01:00:25', 5),
(1587, 'CADASTRO DO CLIENTE ', '', '2015-09-01', '02:06:15', 5),
(1588, 'CADASTRO DO CLIENTE ', '', '2015-09-01', '02:06:59', 5),
(1589, 'CADASTRO DO CLIENTE ', '', '2015-09-01', '02:07:27', 5),
(1590, 'CADASTRO DO CLIENTE ', '', '2015-09-01', '02:07:58', 5),
(1591, 'ALTERAÇÃO DO CLIENTE ', '', '2015-09-01', '03:02:52', 5),
(1592, 'ALTERAÇÃO DO CLIENTE ', '', '2015-09-01', '03:03:13', 5),
(1593, 'CADASTRO DO CLIENTE ', '', '2015-09-01', '03:56:23', 5),
(1594, 'CADASTRO DO CLIENTE ', '', '2015-09-01', '03:56:39', 5),
(1595, 'CADASTRO DO CLIENTE ', '', '2015-09-01', '03:56:47', 5),
(1596, 'CADASTRO DO CLIENTE ', '', '2015-09-01', '03:56:57', 5),
(1597, 'CADASTRO DO CLIENTE ', '', '2015-09-01', '03:57:10', 5),
(1598, 'ALTERAÇÃO DO CLIENTE ', '', '2015-09-01', '03:57:17', 5),
(1599, 'CADASTRO DO CLIENTE ', '', '2015-09-01', '05:00:42', 5),
(1600, 'CADASTRO DO CLIENTE ', '', '2015-09-01', '05:01:39', 5),
(1601, 'CADASTRO DO CLIENTE ', '', '2015-09-01', '05:02:21', 5),
(1602, 'CADASTRO DO CLIENTE ', '', '2015-09-01', '05:54:45', 5),
(1603, 'CADASTRO DO CLIENTE ', '', '2015-09-01', '05:55:32', 5),
(1604, 'CADASTRO DO CLIENTE ', '', '2015-09-01', '05:57:57', 5),
(1605, 'CADASTRO DO CLIENTE ', '', '2015-09-02', '06:59:21', 5),
(1606, 'CADASTRO DO CLIENTE ', '', '2015-09-02', '07:04:45', 5),
(1607, 'CADASTRO DO CLIENTE ', '', '2015-09-02', '07:05:07', 5),
(1608, 'CADASTRO DO CLIENTE ', '', '2015-09-02', '07:05:21', 5),
(1609, 'CADASTRO DO CLIENTE ', '', '2015-09-02', '07:05:36', 5),
(1610, 'CADASTRO DO CLIENTE ', '', '2015-09-02', '07:06:04', 5),
(1611, 'CADASTRO DO CLIENTE ', '', '2015-09-02', '07:06:17', 5),
(1612, 'CADASTRO DO CLIENTE ', '', '2015-09-02', '07:06:34', 5),
(1613, 'ALTERAÇÃO DO CLIENTE ', '', '2015-09-04', '00:22:55', 5),
(1614, 'ALTERAÇÃO DO CLIENTE ', '', '2015-09-04', '00:23:33', 5),
(1615, 'ALTERAÇÃO DO CLIENTE ', '', '2015-09-04', '00:23:48', 5),
(1616, 'ALTERAÇÃO DO CLIENTE ', '', '2015-09-04', '00:24:04', 5);

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_produtos`
--

CREATE TABLE IF NOT EXISTS `tb_produtos` (
  `idproduto` int(10) unsigned NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci NOT NULL,
  `title_google` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `keywords_google` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description_google` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ativo` varchar(3) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'SIM',
  `ordem` int(10) unsigned NOT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `id_categoriaproduto` int(10) unsigned NOT NULL,
  `marca` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `apresentacao` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `avaliacao` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `caracteristicas` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=277 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `tb_produtos`
--

INSERT INTO `tb_produtos` (`idproduto`, `titulo`, `imagem`, `descricao`, `title_google`, `keywords_google`, `description_google`, `ativo`, `ordem`, `url_amigavel`, `id_categoriaproduto`, `marca`, `apresentacao`, `avaliacao`, `caracteristicas`) VALUES
(271, 'Caldeira a lenha', '0109201505008281142417..jpg', '<p>\r\n	Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>\r\n<p>\r\n	Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>\r\n<p>\r\n	Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', '', '', '', 'SIM', 0, 'caldeira-a-lenha', 49, NULL, NULL, NULL, NULL),
(272, 'ENGETÉRMICA', '0109201505016958539821..jpg', '<p>\r\n	Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', '', '', '', 'SIM', 0, 'engetermica', 49, NULL, NULL, NULL, NULL),
(273, 'Vulcanizadora de pneus fixa- Saco de areia', '0109201505021725092828..jpg', '<p>\r\n	Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', '', '', '', 'SIM', 0, 'vulcanizadora-de-pneus-fixa-saco-de-areia', 50, NULL, NULL, NULL, NULL),
(274, 'Exaustores Eólicos', '0109201505546475388941..jpg', '<p>\r\n	Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', '', '', '', 'SIM', 0, 'exaustores-eolicos', 52, NULL, NULL, NULL, NULL),
(275, 'Exaustores/Chaminés de cozinha', '0109201505552055692287..jpg', '<p>\r\n	Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', '', '', '', 'SIM', 0, 'exaustoreschamines-de-cozinha', 52, NULL, NULL, NULL, NULL),
(276, 'Ventiladores / Exaustores', '0109201505577366840466..jpg', '<p>\r\n	Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', '', '', '', 'SIM', 0, 'ventiladores--exaustores', 52, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_servicos`
--

CREATE TABLE IF NOT EXISTS `tb_servicos` (
  `idservico` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci,
  `imagem` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'imagem_nao_disponivel.jpg',
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_categoriaservico` int(10) unsigned NOT NULL,
  `imagem_1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_2` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=40 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `tb_servicos`
--

INSERT INTO `tb_servicos` (`idservico`, `titulo`, `descricao`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `title_google`, `keywords_google`, `description_google`, `id_categoriaservico`, `imagem_1`, `imagem_2`) VALUES
(36, 'Solda MIG/MAG e Arco Submerso', '<p>\r\n	Solda MIG/MAG e Arco Submerso Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab totam odio alias labore fugiat voluptatum atque repella t aperiam eius, odit cupiditate, autem porro ut, consequatur! Amet corrupti blanditiis, alias ab. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veritatis exercitationem, aliquam expedita deserunt iste, quisquam quasi provident reprehenderit cupiditate labore nihil qui repellendus laborum assumenda ullam, amet repellat nesciunt commodi.</p>', '0409201512225175194255..jpg', 'SIM', NULL, 'solda-migmag-e-arco-submerso', '', '', '', 0, '', ''),
(37, 'Corte Plasma CNC', '<p>\r\n	Corte Plasma CNC Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>\r\n<p>\r\n	Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>', '0409201512238643091912..jpg', 'SIM', NULL, 'corte-plasma-cnc', '', '', '', 0, '', ''),
(38, 'Dobra', '<p>\r\n	Dobra&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>\r\n<p>\r\n	Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', '0409201512231394335987..jpg', 'SIM', NULL, 'dobra', '', '', '', 0, '', ''),
(39, 'Montagem de Conjuntos', '<p>\r\n	Montagem de Conjuntos&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>\r\n<p>\r\n	Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', '0409201512245641702165..jpg', 'SIM', NULL, 'montagem-de-conjuntos', '', '', '', 0, '', '');

--
-- Índices de tabelas apagadas
--

--
-- Índices de tabela `tb_banners`
--
ALTER TABLE `tb_banners`
  ADD PRIMARY KEY (`idbanner`);

--
-- Índices de tabela `tb_categorias_produtos`
--
ALTER TABLE `tb_categorias_produtos`
  ADD PRIMARY KEY (`idcategoriaproduto`);

--
-- Índices de tabela `tb_clientes`
--
ALTER TABLE `tb_clientes`
  ADD PRIMARY KEY (`idcliente`);

--
-- Índices de tabela `tb_configuracoes`
--
ALTER TABLE `tb_configuracoes`
  ADD PRIMARY KEY (`idconfiguracao`);

--
-- Índices de tabela `tb_depoimentos`
--
ALTER TABLE `tb_depoimentos`
  ADD PRIMARY KEY (`iddepoimento`);

--
-- Índices de tabela `tb_dicas`
--
ALTER TABLE `tb_dicas`
  ADD PRIMARY KEY (`iddica`);

--
-- Índices de tabela `tb_empresa`
--
ALTER TABLE `tb_empresa`
  ADD PRIMARY KEY (`idempresa`);

--
-- Índices de tabela `tb_galerias_produtos`
--
ALTER TABLE `tb_galerias_produtos`
  ADD PRIMARY KEY (`id_galeriaproduto`);

--
-- Índices de tabela `tb_galerias_servicos`
--
ALTER TABLE `tb_galerias_servicos`
  ADD PRIMARY KEY (`idgaleriaservico`);

--
-- Índices de tabela `tb_logins`
--
ALTER TABLE `tb_logins`
  ADD PRIMARY KEY (`idlogin`);

--
-- Índices de tabela `tb_logs_logins`
--
ALTER TABLE `tb_logs_logins`
  ADD PRIMARY KEY (`idloglogin`);

--
-- Índices de tabela `tb_produtos`
--
ALTER TABLE `tb_produtos`
  ADD PRIMARY KEY (`idproduto`);

--
-- Índices de tabela `tb_servicos`
--
ALTER TABLE `tb_servicos`
  ADD PRIMARY KEY (`idservico`);

--
-- AUTO_INCREMENT de tabelas apagadas
--

--
-- AUTO_INCREMENT de tabela `tb_banners`
--
ALTER TABLE `tb_banners`
  MODIFY `idbanner` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT de tabela `tb_categorias_produtos`
--
ALTER TABLE `tb_categorias_produtos`
  MODIFY `idcategoriaproduto` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=54;
--
-- AUTO_INCREMENT de tabela `tb_clientes`
--
ALTER TABLE `tb_clientes`
  MODIFY `idcliente` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT de tabela `tb_configuracoes`
--
ALTER TABLE `tb_configuracoes`
  MODIFY `idconfiguracao` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de tabela `tb_depoimentos`
--
ALTER TABLE `tb_depoimentos`
  MODIFY `iddepoimento` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de tabela `tb_dicas`
--
ALTER TABLE `tb_dicas`
  MODIFY `iddica` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=50;
--
-- AUTO_INCREMENT de tabela `tb_empresa`
--
ALTER TABLE `tb_empresa`
  MODIFY `idempresa` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de tabela `tb_galerias_produtos`
--
ALTER TABLE `tb_galerias_produtos`
  MODIFY `id_galeriaproduto` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=89;
--
-- AUTO_INCREMENT de tabela `tb_galerias_servicos`
--
ALTER TABLE `tb_galerias_servicos`
  MODIFY `idgaleriaservico` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT de tabela `tb_logins`
--
ALTER TABLE `tb_logins`
  MODIFY `idlogin` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de tabela `tb_logs_logins`
--
ALTER TABLE `tb_logs_logins`
  MODIFY `idloglogin` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1617;
--
-- AUTO_INCREMENT de tabela `tb_produtos`
--
ALTER TABLE `tb_produtos`
  MODIFY `idproduto` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=277;
--
-- AUTO_INCREMENT de tabela `tb_servicos`
--
ALTER TABLE `tb_servicos`
  MODIFY `idservico` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=40;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
