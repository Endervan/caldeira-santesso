-- MySQL dump 10.13  Distrib 5.5.38-35.2, for Linux (x86_64)
--
-- Host: localhost    Database: masmidia_caldeira_santesso
-- ------------------------------------------------------
-- Server version	5.5.38-35.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tb_banners`
--

DROP TABLE IF EXISTS `tb_banners`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_banners` (
  `idbanner` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ativo` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `tipo_banner` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`idbanner`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_banners`
--

LOCK TABLES `tb_banners` WRITE;
/*!40000 ALTER TABLE `tb_banners` DISABLE KEYS */;
INSERT INTO `tb_banners` (`idbanner`, `titulo`, `imagem`, `ativo`, `ordem`, `tipo_banner`, `url_amigavel`, `url`) VALUES (34,'Banner 1','0409201509193770884577.jpg','NAO',NULL,'1','banner-1',''),(35,'Banner 2','0412201512598809632195.jpg','NAO',NULL,'1','banner-2',''),(36,'Banner 3','0412201512144712373465.jpg','SIM',NULL,'1','banner-3',''),(37,'Mobile - Banner 1','1910201505048629793975.jpg','SIM',NULL,'2','mobile--banner-1',''),(38,'Mobile - Banner 2','1910201505044406066474.jpg','SIM',NULL,'2','mobile--banner-2','');
/*!40000 ALTER TABLE `tb_banners` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_categorias_produtos`
--

DROP TABLE IF EXISTS `tb_categorias_produtos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_categorias_produtos` (
  `idcategoriaproduto` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `desCategoria_Dica_Modelcricao` longtext COLLATE utf8_unicode_ci,
  `imagem` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'imagem_nao_disponivel.jpg',
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `classe` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`idcategoriaproduto`)
) ENGINE=MyISAM AUTO_INCREMENT=58 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_categorias_produtos`
--

LOCK TABLES `tb_categorias_produtos` WRITE;
/*!40000 ALTER TABLE `tb_categorias_produtos` DISABLE KEYS */;
INSERT INTO `tb_categorias_produtos` (`idcategoriaproduto`, `titulo`, `desCategoria_Dica_Modelcricao`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `title_google`, `keywords_google`, `description_google`, `classe`) VALUES (49,'CALDEIRAS',NULL,'imagem_nao_disponivel.jpg','SIM',NULL,'caldeiras',NULL,NULL,NULL,NULL),(50,'VULCANIZADORAS',NULL,'imagem_nao_disponivel.jpg','SIM',NULL,'vulcanizadoras',NULL,NULL,NULL,NULL),(51,'FILTRO CAPTADOR',NULL,'imagem_nao_disponivel.jpg','SIM',NULL,'filtro-captador',NULL,NULL,NULL,NULL),(52,'EXAUSTORES',NULL,'imagem_nao_disponivel.jpg','SIM',NULL,'exaustores',NULL,NULL,NULL,NULL),(53,'FORNALHAS',NULL,'imagem_nao_disponivel.jpg','SIM',NULL,'fornalhas',NULL,NULL,NULL,NULL),(54,'BOILER AQUECEDOR',NULL,'imagem_nao_disponivel.jpg','SIM',NULL,'boiler-aquecedor',NULL,NULL,NULL,NULL),(55,'TACHOS',NULL,'imagem_nao_disponivel.jpg','SIM',NULL,'tachos',NULL,NULL,NULL,NULL),(56,'LAVADORAS DE GASES',NULL,'imagem_nao_disponivel.jpg','SIM',NULL,'lavadoras-de-gases',NULL,NULL,NULL,NULL),(57,'QUEIMADORES',NULL,'imagem_nao_disponivel.jpg','SIM',NULL,'queimadores',NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `tb_categorias_produtos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_clientes`
--

DROP TABLE IF EXISTS `tb_clientes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_clientes` (
  `idcliente` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricao` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ativo` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `title_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_google` longtext COLLATE utf8_unicode_ci,
  `keywords_google` longtext COLLATE utf8_unicode_ci,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`idcliente`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_clientes`
--

LOCK TABLES `tb_clientes` WRITE;
/*!40000 ALTER TABLE `tb_clientes` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_clientes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_configuracoes`
--

DROP TABLE IF EXISTS `tb_configuracoes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_configuracoes` (
  `idconfiguracao` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title_google` longtext NOT NULL,
  `description_google` longtext NOT NULL,
  `keywords_google` longtext NOT NULL,
  `ativo` varchar(3) NOT NULL DEFAULT 'SIM',
  `ordem` int(10) unsigned NOT NULL,
  `url_amigavel` varchar(255) NOT NULL,
  `endereco` varchar(255) NOT NULL,
  `telefone1` varchar(255) NOT NULL,
  `telefone2` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `src_place` varchar(255) NOT NULL,
  `horario_1` varchar(255) DEFAULT NULL,
  `horario_2` varchar(255) DEFAULT NULL,
  `email_copia` varchar(255) DEFAULT NULL,
  `telefone3` varchar(255) DEFAULT NULL,
  `telefone4` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idconfiguracao`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_configuracoes`
--

LOCK TABLES `tb_configuracoes` WRITE;
/*!40000 ALTER TABLE `tb_configuracoes` DISABLE KEYS */;
INSERT INTO `tb_configuracoes` (`idconfiguracao`, `title_google`, `description_google`, `keywords_google`, `ativo`, `ordem`, `url_amigavel`, `endereco`, `telefone1`, `telefone2`, `email`, `src_place`, `horario_1`, `horario_2`, `email_copia`, `telefone3`, `telefone4`) VALUES (1,'Caldeira Santesso - Caldeiras, Geradores e Equipamentos Industriais - Goiânia - Goiás','Caldeira Santesso - Caldeiras, Geradores e Equipamentos Industriais - Goiânia - Goiás','Caldeira Santesso - Caldeiras, Geradores e Equipamentos Industriais - Goiânia - Goiás','SIM',0,'caldeira-santesso--caldeiras-geradores-e-equipamentos-industriais--goiania--goias','Rua 28, Qd.84-A Lts. 15-16 - Vila Brasília - Goiânia - GO - Brasil','(62) 3249-0182','(61)3452-1111','caldeirasantesso@hotmail.com','https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d122265.44978181062!2d-49.310793493002706!3d-16.737062193829942!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x935ef07ca87c819d%3A0x94bdd4675008a813!2sCaldeiras+Santesso!5e0!3m2!1spt-BR!2sbr!4v1448',NULL,NULL,'','(61)3426-0543','(61)3452-1236');
/*!40000 ALTER TABLE `tb_configuracoes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_depoimentos`
--

DROP TABLE IF EXISTS `tb_depoimentos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_depoimentos` (
  `iddepoimento` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cargo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `depoimento` longtext COLLATE utf8_unicode_ci,
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`iddepoimento`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_depoimentos`
--

LOCK TABLES `tb_depoimentos` WRITE;
/*!40000 ALTER TABLE `tb_depoimentos` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_depoimentos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_dicas`
--

DROP TABLE IF EXISTS `tb_dicas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_dicas` (
  `iddica` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci,
  `imagem` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'imagem_nao_disponivel.jpg',
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` date DEFAULT NULL,
  PRIMARY KEY (`iddica`)
) ENGINE=MyISAM AUTO_INCREMENT=50 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_dicas`
--

LOCK TABLES `tb_dicas` WRITE;
/*!40000 ALTER TABLE `tb_dicas` DISABLE KEYS */;
INSERT INTO `tb_dicas` (`iddica`, `titulo`, `descricao`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `title_google`, `keywords_google`, `description_google`, `data`) VALUES (42,'MODELO F2 VISTA FRONTAL','<p>\r\n	PRINCIPAIS VANTAGENS DAS CALDEIRAS SANTESSO</p>\r\n<p>\r\n	As caldeiras s&atilde;o projetadas e executadas com know-how pr&oacute;prio, o que nos permite um atendimento r&aacute;pido e preciso.</p>\r\n<p>\r\n	Mais de 530 unidades j&aacute; fabricadas e instaladas sob nosso know-how, nas mais diversas ind&uacute;strias do pa&iacute;s.</p>\r\n<p>\r\n	As caldeiras s&atilde;o projetadas conforme normas t&eacute;cnicas da ABNT e obedecem especifica&ccedil;&otilde;es da NB-55 e NR-13</p>\r\n<p>\r\n	Menor &iacute;ndice de ru&iacute;dos, abaixo dos limites permiss&iacute;veis por lei.</p>\r\n<p>\r\n	Sistema de tiragem induzida dos gases de combust&atilde;o, permitindo adapta&ccedil;&atilde;o de purificadores e lavadores destes gases.</p>\r\n<p>\r\n	Corpo de engenheiros e t&eacute;cnicos para completa cobertura nas vendas, instala&ccedil;&atilde;o e manuten&ccedil;&atilde;o dos equipamentos.</p>\r\n<p>\r\n	Produ&ccedil;&atilde;o de vapor &nbsp;at&eacute; 20ton/h, sendo que acima de 10ton/h as nossas caldeiras s&atilde;o fornecidas com fornalha dupla.</p>\r\n<p>\r\n	Simples adapta&ccedil;&atilde;o de ante-fornalhas, permitindo a queima dos mais diversos tipos de combust&iacute;veis s&oacute;lidos.</p>\r\n<p>\r\n	Assist&ecirc;ncia t&eacute;cnica gratuita no per&iacute;odo da garantia.</p>','2511201511386193337523..jpg','SIM',NULL,'modelo-f2-vista-frontal',NULL,NULL,NULL,NULL),(43,'MODELO F2 VISTA DE CIMA','<p>\r\n	PRINCIPAIS VANTAGENS DAS CALDEIRAS SANTESSO</p>\r\n<p>\r\n	As caldeiras s&atilde;o projetadas e executadas com know-how pr&oacute;prio, o que nos permite um atendimento r&aacute;pido e preciso.</p>\r\n<p>\r\n	Mais de 530 unidades j&aacute; fabricadas e instaladas sob nosso know-how, nas mais diversas ind&uacute;strias do pa&iacute;s.</p>\r\n<p>\r\n	As caldeiras s&atilde;o projetadas conforme normas t&eacute;cnicas da ABNT e obedecem especifica&ccedil;&otilde;es da NB-55 e NR-13</p>\r\n<p>\r\n	Menor &iacute;ndice de ru&iacute;dos, abaixo dos limites permiss&iacute;veis por lei.</p>\r\n<p>\r\n	Sistema de tiragem induzida dos gases de combust&atilde;o, permitindo adapta&ccedil;&atilde;o de purificadores e lavadores destes gases.</p>\r\n<p>\r\n	Corpo de engenheiros e t&eacute;cnicos para completa cobertura nas vendas, instala&ccedil;&atilde;o e manuten&ccedil;&atilde;o dos equipamentos.</p>\r\n<p>\r\n	Produ&ccedil;&atilde;o de vapor &nbsp;at&eacute; 20ton/h, sendo que acima de 10ton/h as nossas caldeiras s&atilde;o fornecidas com fornalha dupla.</p>\r\n<p>\r\n	Simples adapta&ccedil;&atilde;o de ante-fornalhas, permitindo a queima dos mais diversos tipos de combust&iacute;veis s&oacute;lidos.</p>\r\n<p>\r\n	Assist&ecirc;ncia t&eacute;cnica gratuita no per&iacute;odo da garantia.</p>','2511201511402008095073..jpg','SIM',NULL,'modelo-f2-vista-de-cima',NULL,NULL,NULL,NULL),(44,'MODELO F2 VISTA LATERAL','<p>\r\n	PRINCIPAIS VANTAGENS DAS CALDEIRAS SANTESSO</p>\r\n<p>\r\n	As caldeiras s&atilde;o projetadas e executadas com know-how pr&oacute;prio, o que nos permite um atendimento r&aacute;pido e preciso.</p>\r\n<p>\r\n	Mais de 530 unidades j&aacute; fabricadas e instaladas sob nosso know-how, nas mais diversas ind&uacute;strias do pa&iacute;s.</p>\r\n<p>\r\n	As caldeiras s&atilde;o projetadas conforme normas t&eacute;cnicas da ABNT e obedecem especifica&ccedil;&otilde;es da NB-55 e NR-13</p>\r\n<p>\r\n	Menor &iacute;ndice de ru&iacute;dos, abaixo dos limites permiss&iacute;veis por lei.</p>\r\n<p>\r\n	Sistema de tiragem induzida dos gases de combust&atilde;o, permitindo adapta&ccedil;&atilde;o de purificadores e lavadores destes gases.</p>\r\n<p>\r\n	Corpo de engenheiros e t&eacute;cnicos para completa cobertura nas vendas, instala&ccedil;&atilde;o e manuten&ccedil;&atilde;o dos equipamentos.</p>\r\n<p>\r\n	Produ&ccedil;&atilde;o de vapor &nbsp;at&eacute; 20ton/h, sendo que acima de 10ton/h as nossas caldeiras s&atilde;o fornecidas com fornalha dupla.</p>\r\n<p>\r\n	Simples adapta&ccedil;&atilde;o de ante-fornalhas, permitindo a queima dos mais diversos tipos de combust&iacute;veis s&oacute;lidos.</p>\r\n<p>\r\n	Assist&ecirc;ncia t&eacute;cnica gratuita no per&iacute;odo da garantia.</p>','2511201511423743348928..jpg','SIM',NULL,'modelo-f2-vista-lateral',NULL,NULL,NULL,NULL),(45,'CALDEIRA QUEIMA MISTA','<p>\r\n	PRINCIPAIS VANTAGENS DAS CALDEIRAS SANTESSO</p>\r\n<p>\r\n	As caldeiras s&atilde;o projetadas e executadas com know-how pr&oacute;prio, o que nos permite um atendimento r&aacute;pido e preciso.</p>\r\n<p>\r\n	Mais de 530 unidades j&aacute; fabricadas e instaladas sob nosso know-how, nas mais diversas ind&uacute;strias do pa&iacute;s.</p>\r\n<p>\r\n	As caldeiras s&atilde;o projetadas conforme normas t&eacute;cnicas da ABNT e obedecem especifica&ccedil;&otilde;es da NB-55 e NR-13</p>\r\n<p>\r\n	Menor &iacute;ndice de ru&iacute;dos, abaixo dos limites permiss&iacute;veis por lei.</p>\r\n<p>\r\n	Sistema de tiragem induzida dos gases de combust&atilde;o, permitindo adapta&ccedil;&atilde;o de purificadores e lavadores destes gases.</p>\r\n<p>\r\n	Corpo de engenheiros e t&eacute;cnicos para completa cobertura nas vendas, instala&ccedil;&atilde;o e manuten&ccedil;&atilde;o dos equipamentos.</p>\r\n<p>\r\n	Produ&ccedil;&atilde;o de vapor &nbsp;at&eacute; 20ton/h, sendo que acima de 10ton/h as nossas caldeiras s&atilde;o fornecidas com fornalha dupla.</p>\r\n<p>\r\n	Simples adapta&ccedil;&atilde;o de ante-fornalhas, permitindo a queima dos mais diversos tipos de combust&iacute;veis s&oacute;lidos.</p>\r\n<p>\r\n	Assist&ecirc;ncia t&eacute;cnica gratuita no per&iacute;odo da garantia.</p>','2511201511441996772947..jpg','SIM',NULL,'caldeira-queima-mista',NULL,NULL,NULL,NULL),(46,'Dica 5','<p>\r\n	5&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>','0209201507051499702127..jpg','NAO',NULL,'dica-5',NULL,NULL,NULL,NULL),(48,'Dica 7','<p>\r\n	7&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>','0209201507065624973852..jpg','NAO',NULL,'dica-7',NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `tb_dicas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_empresa`
--

DROP TABLE IF EXISTS `tb_empresa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_empresa` (
  `idempresa` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titulo` varchar(80) NOT NULL,
  `descricao` longtext NOT NULL,
  `ativo` varchar(3) NOT NULL DEFAULT 'SIM',
  `ordem` int(10) unsigned NOT NULL,
  `title_google` varchar(255) NOT NULL,
  `keywords_google` varchar(255) NOT NULL,
  `description_google` varchar(255) NOT NULL,
  `url_amigavel` varchar(255) NOT NULL,
  `colaboradores_especializados` int(11) DEFAULT NULL,
  `trabalhos_entregues` int(11) DEFAULT NULL,
  `trabalhos_entregues_este_mes` int(11) DEFAULT NULL,
  PRIMARY KEY (`idempresa`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_empresa`
--

LOCK TABLES `tb_empresa` WRITE;
/*!40000 ALTER TABLE `tb_empresa` DISABLE KEYS */;
INSERT INTO `tb_empresa` (`idempresa`, `titulo`, `descricao`, `ativo`, `ordem`, `title_google`, `keywords_google`, `description_google`, `url_amigavel`, `colaboradores_especializados`, `trabalhos_entregues`, `trabalhos_entregues_este_mes`) VALUES (1,'Empresa - Legenda','<div>\r\n	Especializada na fabrica&ccedil;&atilde;o e instala&ccedil;&atilde;o de equipamentos industriais e caldeiras a vapor</div>','SIM',0,'','','','empresa--legenda',NULL,NULL,NULL),(2,'Empresa - Nossa Missão','<p>\r\n	A CALDEIRAS SANTESSO, FUNDADA EM 1977, &Eacute; ESPECIALIZADA EM FABRICA&Ccedil;&Atilde;O E INSTALA&Ccedil;&Atilde;O DE EQUIPAMENTOS INDUSTRIAIS E CALDEIRAS GERADORAS DE VAPOR DE V&Aacute;RIOS MODELOS, DIRECIONADOS PARA OS M&Uacute;LTIPLOS SETORES INDUSTRIAIS, ALIMENT&Iacute;CIOS, HOTELEIROS E OUTROS, PARALELOS A ESTES.</p>\r\n<p>\r\n	NOSSA EXPERI&Ecirc;NCIA DE 34 ANOS ATUANDO NO MERCADO, VISA ACIMA DE TUDO A QUALIDADE E SEGURAN&Ccedil;A DE NOSSOS EQUIPAMENTOS, AL&Eacute;M DE GARANTIR A ECONOMIA DE ENERGIA, DURABILIDADE E O RESULTADO ESPERADO. SEMPRE NOS ORGULHAMOS COM A QUALIDADE DOS NOSSOS SERVI&Ccedil;OS, QUE &Eacute; FRUTO DA APROVA&Ccedil;&Atilde;O E SATISFA&Ccedil;&Atilde;O DE NOSSOS CLIENTES.</p>\r\n<p>\r\n	A CALDEIRAS SANTESSO DESENVOLVE UM TRABALHO S&Eacute;RIO, COM MATERIAIS CLASSIFICADOS E UMA EXPERIENTE EQUIPE QUE VIVE EM PROCESSO CONT&Iacute;NUO DE ATUALIZA&Ccedil;&Atilde;O T&Eacute;CNICA, DESENVOLVENDO SOLU&Ccedil;&Otilde;ES SEMPRE DE ACORDO COM AS NORMAS DA ABNT. COMERCIALIZAMOS PRODUTOS DA MAIS ALTA QUALIDADE, CAPAZ DE ATENDER AS MAIS R&Iacute;GIDAS EXIG&Ecirc;NCIAS DE NOSSOS CLIENTES, EM TODOS OS SEGMENTOS ONDE ATUAMOS. NOSSA EMPRESA ACEITA FINANCIAMENTO PR&Oacute;PRIO PELO BNDS E FINAME.</p>','SIM',0,'','','','empresa--nossa-missao',NULL,NULL,NULL),(3,'Empresa - Nossa Visão','<div ainda=\\\"\\\\&quot;\\\\&quot;\\\" comprar=\\\"\\\\&quot;\\\\&quot;\\\" div=\\\"\\\\&quot;\\\\&quot;\\\" mais=\\\"\\\\&quot;\\\\&quot;\\\" os=\\\"\\\\&quot;\\\\&quot;\\\" para=\\\"\\\\&quot;\\\\&quot;\\\" produtos=\\\"\\\\&quot;\\\\&quot;\\\" santesso=\\\"\\\\&quot;\\\\&quot;\\\" style=\\\"\\\\&quot;\\\\\\\\&quot;text-align:\\\\&quot;\\\" vantagens=\\\"\\\\&quot;\\\\&quot;\\\">\r\n	<p>\r\n		<strong>AINDA MAIS VANTAGENS PARA COMPRAR OS PRODUTOS SANTESSO !</strong></p>\r\n	<p>\r\n		<strong>VANTAGENS DO CART&Atilde;O BNDES:</strong></p>\r\n	<p>\r\n		&bull; FINANCIAMENTO AUTOM&Aacute;TICO EM AT&Eacute; 36 MESES</p>\r\n	<p>\r\n		&bull; PRESTA&Ccedil;&Otilde;ES FIXAS E IGUAIS</p>\r\n	<p>\r\n		&bull; TAXA DE JUROS ATRATIVA (INFORMADA NA P&Aacute;GINA INICIAL DO PORTAL WWW.CARTAOBNDES.GOV.BR)</p>\r\n	<p>\r\n		<strong>O QUE &Eacute; O CART&Atilde;O BNDES ?</strong></p>\r\n	<p>\r\n		O CART&Atilde;O BNDES &Eacute; UM PRODUTO QUE, BASEADO NO CONCEITO DE CART&Atilde;O DE CR&Eacute;DITO, VISA FINANCIAR OS INVESTIMENTOS DAS MICRO, PEQUENAS E M&Eacute;DIAS EMPRESAS (MPMES).</p>\r\n	<p>\r\n		PODEM OBTER O CART&Atilde;O BNDES AS MPMES (COM FATURAMENTO BRUTO ANUAL DE AT&Eacute; R$ 60 MILH&Otilde;ES), SEDIADAS NO PA&Iacute;S, QUE EXER&Ccedil;AM ATIVIDADE ECON&Ocirc;MICA COMPAT&Iacute;VEIS COM AS POL&Iacute;TICAS OPERACIONAIS E DE CR&Eacute;DITO DO BNDES E QUE ESTEJAM EM DIA COM O INSS, FGTS, RAIS E TRIBUTOS FEDERAIS.</p>\r\n	<p>\r\n		O PORTADOR DO CART&Atilde;O BNDES EFETUAR&Aacute; SUA COMPRA, EXCLUSIVAMENTE NO &Acirc;MBITO DO PORTAL DE OPERA&Ccedil;&Otilde;ES DO BNDES (WWW.CARTAOBNDES.GOV.BR), PROCURANDO OS PRODUTOS QUE LHE INTERESSAM NO CAT&Aacute;LOGO DE PRODUTOS EXPOSTOS E SEGUINDO OS PASSOS INDICADOS PARA A COMPRA.</p>\r\n	<p>\r\n		O BRADESCO, O BANCO DO BRASIL, A CAIXA ECON&Ocirc;MICA FEDERAL E A NOSSA CAIXA S&Atilde;O, ATUALMENTE, OS BANCOS EMISSORES DO CART&Atilde;O BNDES E A VISA E MASTERCARD AS BANDEIRAS DE CART&Atilde;O DE CR&Eacute;DITO.&nbsp;</p>\r\n	<p>\r\n		<strong>AS CONDI&Ccedil;&Otilde;ES FINANCEIRAS EM VIGOR S&Atilde;O:</strong></p>\r\n	<p>\r\n		&bull; LIMITE DE CR&Eacute;DITO DE AT&Eacute; R$ 500 MIL POR CART&Atilde;O, POR BANCO EMISSOR*</p>\r\n	<p>\r\n		&bull; PRAZO DE PARCELAMENTO DE 3 A 48 MESES**</p>\r\n	<p>\r\n		&bull; TAXA DE JUROS PR&Eacute;-FIXADA (INFORMADA NA P&Aacute;GINA INICIAL DO PORTAL)</p>\r\n	<p>\r\n		*OBS 1: O LIMITE DE CR&Eacute;DITO DE CADA CLIENTE SER&Aacute; ATRIBU&Iacute;DO PELO BANCO EMISSOR DO CART&Atilde;O, AP&Oacute;S A RESPECTIVA AN&Aacute;LISE DE CR&Eacute;DITO. UMA EMPRESA PODE OBTER UM CART&Atilde;O BNDES POR BANCO EMISSOR, PODENDO TER AT&Eacute; 4 CART&Otilde;ES E SOMAR SEUS LIMITES NUMA &Uacute;NICA TRANSA&Ccedil;&Atilde;O. OS CART&Otilde;ES BNDES EMITIDOS PELA CAIXA ECON&Ocirc;MICA FEDERAL/MASTERCARD PODEM OBTER O LIMITE INDIVIDUAL DE AT&Eacute; R$ 250 MIL.</p>\r\n	<p>\r\n		**OBS 2: OS CART&Otilde;ES BNDES EMITIDOS PELA CAIXA ECON&Ocirc;MICA FEDERAL/MASTERCARD AINDA N&Atilde;O PERMITEM ESTE PARCELAMENTO, O QUE DEVER&Aacute; OCORRER EM BREVE, E ACEITAM APENAS AS CONDI&Ccedil;&Otilde;ES DE PARCELAMENTO EM 12, 18, 24 OU 36 PARCELAS.</p>\r\n	<p>\r\n		<strong>COMO SOLICITAR O CART&Atilde;O BNDES ?</strong></p>\r\n	<p>\r\n		PELA INTERNET: DIGITE O ENDERE&Ccedil;O WWW.CARTAOBNDES.GOV.BR, CLIQUE EM &ldquo;SOLICITE SEU CART&Atilde;O BNDES&rdquo; E SIGA AS ORIENTA&Ccedil;&Otilde;ES FORNECIDAS PELO PORTAL DE OPERA&Ccedil;&Otilde;ES DO BNDES.**</p>\r\n	<p>\r\n		<strong>PARA MAIS INFORMA&Ccedil;&Otilde;ES ACESSE O LINK: WWW.CARTAOBNDES.GOV.BR</strong></p>\r\n</div>\r\n<p>\r\n	&nbsp;</p>','SIM',0,'','','','empresa--nossa-visao',NULL,NULL,NULL),(4,'Empresa - Nossos Valores','','SIM',0,'','','','empresa--nossos-valores',NULL,NULL,NULL),(5,'Serviços - Legenda','<div>\r\n	Servi&ccedil;os com qualidade desenvolvendo solu&ccedil;&otilde;es sempre de acordo com as normas da ABNT</div>','SIM',0,'','','','servicos--legenda',NULL,NULL,NULL),(6,'Produtos - Legenda','<div>\r\n	Comercializamos produtos da mais alta qualidade, capaz de atender &agrave;s mais r&iacute;gidas exig&ecirc;ncias de nossos clientes</div>','SIM',0,'','','','produtos--legenda',NULL,NULL,NULL),(7,'Orçamento - Legenda','<div>\r\n	Entre em contato conosco. Solicite informa&ccedil;&otilde;es sobre nossos produtos e servi&ccedil;os</div>','SIM',0,'','','','orcamento--legenda',NULL,NULL,NULL);
/*!40000 ALTER TABLE `tb_empresa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_galerias_produtos`
--

DROP TABLE IF EXISTS `tb_galerias_produtos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_galerias_produtos` (
  `id_galeriaproduto` int(11) NOT NULL AUTO_INCREMENT,
  `imagem` varchar(255) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `id_produto` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_galeriaproduto`)
) ENGINE=InnoDB AUTO_INCREMENT=115 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_galerias_produtos`
--

LOCK TABLES `tb_galerias_produtos` WRITE;
/*!40000 ALTER TABLE `tb_galerias_produtos` DISABLE KEYS */;
INSERT INTO `tb_galerias_produtos` (`id_galeriaproduto`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `id_produto`) VALUES (89,'1811201508097440284685.jpg','SIM',NULL,NULL,271),(90,'1811201508131152061335.jpg','SIM',NULL,NULL,272),(91,'1811201508156716797198.jpg','SIM',NULL,NULL,273),(92,'1811201508183306418193.jpg','SIM',NULL,NULL,274),(93,'1811201508203279376605.jpg','SIM',NULL,NULL,275),(94,'1811201508237052664756.jpg','SIM',NULL,NULL,276),(95,'1811201508276792773447.jpg','SIM',NULL,NULL,277),(96,'1811201508272788442683.jpg','SIM',NULL,NULL,277),(97,'1811201508277182437880.jpg','SIM',NULL,NULL,277),(98,'1811201508316827209645.jpg','SIM',NULL,NULL,278),(99,'1811201508311172017604.jpg','SIM',NULL,NULL,278),(100,'1811201508367242294169.jpg','SIM',NULL,NULL,279),(101,'1811201508367407606976.jpg','SIM',NULL,NULL,279),(102,'1811201508361362948777.jpg','SIM',NULL,NULL,279),(103,'1811201508363591059346.jpg','SIM',NULL,NULL,279),(104,'1811201508364616947426.jpg','SIM',NULL,NULL,279),(105,'1811201508408311533199.jpg','SIM',NULL,NULL,280),(106,'1811201508408971408824.jpg','SIM',NULL,NULL,280),(107,'1811201508406879566361.jpg','SIM',NULL,NULL,280),(108,'1811201508442549543215.jpg','SIM',NULL,NULL,281),(109,'1811201508488996803810.jpg','SIM',NULL,NULL,282),(110,'1811201508488858215734.jpg','SIM',NULL,NULL,282),(111,'1811201508481439889698.jpg','SIM',NULL,NULL,282),(112,'1811201508524044652626.jpg','SIM',NULL,NULL,283),(113,'1811201508564983585770.jpg','SIM',NULL,NULL,284),(114,'1811201508569434761380.jpg','SIM',NULL,NULL,284);
/*!40000 ALTER TABLE `tb_galerias_produtos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_galerias_servicos`
--

DROP TABLE IF EXISTS `tb_galerias_servicos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_galerias_servicos` (
  `idgaleriaservico` int(11) NOT NULL AUTO_INCREMENT,
  `id_servico` int(11) DEFAULT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  PRIMARY KEY (`idgaleriaservico`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_galerias_servicos`
--

LOCK TABLES `tb_galerias_servicos` WRITE;
/*!40000 ALTER TABLE `tb_galerias_servicos` DISABLE KEYS */;
INSERT INTO `tb_galerias_servicos` (`idgaleriaservico`, `id_servico`, `imagem`, `ativo`, `ordem`) VALUES (6,37,'0209201510378591811392.jpg','SIM',NULL),(7,37,'0209201510374092702898.jpeg','SIM',NULL),(8,37,'0209201510374217629743.jpg','SIM',NULL),(9,37,'0209201510371998167745.jpg','SIM',NULL),(10,37,'0209201510376213156084.jpg','SIM',NULL),(11,38,'0209201510371771991078.jpg','SIM',NULL),(12,38,'0209201510375567460267.jpeg','SIM',NULL),(13,38,'0209201510373327259189.jpg','SIM',NULL),(14,39,'0209201510377202819854.jpg','SIM',NULL),(15,39,'0209201510373317656338.jpeg','SIM',NULL),(16,39,'0209201510373725799442.jpg','SIM',NULL),(17,39,'0209201510377658288880.jpg','SIM',NULL),(18,38,'0209201510376385150117.jpg','SIM',NULL),(19,39,'0209201510373384791943.jpg','SIM',NULL),(20,38,'0209201510377415764157.jpg','SIM',NULL);
/*!40000 ALTER TABLE `tb_galerias_servicos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_logins`
--

DROP TABLE IF EXISTS `tb_logins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_logins` (
  `idlogin` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) DEFAULT NULL,
  `senha` varchar(45) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `id_grupologin` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  PRIMARY KEY (`idlogin`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_logins`
--

LOCK TABLES `tb_logins` WRITE;
/*!40000 ALTER TABLE `tb_logins` DISABLE KEYS */;
INSERT INTO `tb_logins` (`idlogin`, `nome`, `senha`, `ativo`, `id_grupologin`, `email`) VALUES (5,'HomeWeb','e10adc3949ba59abbe56e057f20f883e','SIM',0,'atendimento.sites@homewebbrasil.com.br');
/*!40000 ALTER TABLE `tb_logins` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_logs_logins`
--

DROP TABLE IF EXISTS `tb_logs_logins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_logs_logins` (
  `idloglogin` int(11) NOT NULL AUTO_INCREMENT,
  `operacao` longtext,
  `consulta_sql` longtext,
  `data` date DEFAULT NULL,
  `hora` time DEFAULT NULL,
  `id_login` int(11) NOT NULL,
  PRIMARY KEY (`idloglogin`)
) ENGINE=InnoDB AUTO_INCREMENT=1681 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_logs_logins`
--

LOCK TABLES `tb_logs_logins` WRITE;
/*!40000 ALTER TABLE `tb_logs_logins` DISABLE KEYS */;
INSERT INTO `tb_logs_logins` (`idloglogin`, `operacao`, `consulta_sql`, `data`, `hora`, `id_login`) VALUES (1580,'CADASTRO DO CLIENTE ','','2015-09-01','00:15:30',5),(1581,'CADASTRO DO CLIENTE ','','2015-09-01','00:15:39',5),(1582,'CADASTRO DO CLIENTE ','','2015-09-01','00:15:45',5),(1583,'CADASTRO DO CLIENTE ','','2015-09-01','00:15:53',5),(1584,'CADASTRO DO CLIENTE ','','2015-09-01','00:16:02',5),(1585,'CADASTRO DO CLIENTE ','','2015-09-01','00:58:04',5),(1586,'CADASTRO DO CLIENTE ','','2015-09-01','01:00:25',5),(1587,'CADASTRO DO CLIENTE ','','2015-09-01','02:06:15',5),(1588,'CADASTRO DO CLIENTE ','','2015-09-01','02:06:59',5),(1589,'CADASTRO DO CLIENTE ','','2015-09-01','02:07:27',5),(1590,'CADASTRO DO CLIENTE ','','2015-09-01','02:07:58',5),(1591,'ALTERAÇÃO DO CLIENTE ','','2015-09-01','03:02:52',5),(1592,'ALTERAÇÃO DO CLIENTE ','','2015-09-01','03:03:13',5),(1593,'CADASTRO DO CLIENTE ','','2015-09-01','03:56:23',5),(1594,'CADASTRO DO CLIENTE ','','2015-09-01','03:56:39',5),(1595,'CADASTRO DO CLIENTE ','','2015-09-01','03:56:47',5),(1596,'CADASTRO DO CLIENTE ','','2015-09-01','03:56:57',5),(1597,'CADASTRO DO CLIENTE ','','2015-09-01','03:57:10',5),(1598,'ALTERAÇÃO DO CLIENTE ','','2015-09-01','03:57:17',5),(1599,'CADASTRO DO CLIENTE ','','2015-09-01','05:00:42',5),(1600,'CADASTRO DO CLIENTE ','','2015-09-01','05:01:39',5),(1601,'CADASTRO DO CLIENTE ','','2015-09-01','05:02:21',5),(1602,'CADASTRO DO CLIENTE ','','2015-09-01','05:54:45',5),(1603,'CADASTRO DO CLIENTE ','','2015-09-01','05:55:32',5),(1604,'CADASTRO DO CLIENTE ','','2015-09-01','05:57:57',5),(1605,'CADASTRO DO CLIENTE ','','2015-09-02','06:59:21',5),(1606,'CADASTRO DO CLIENTE ','','2015-09-02','07:04:45',5),(1607,'CADASTRO DO CLIENTE ','','2015-09-02','07:05:07',5),(1608,'CADASTRO DO CLIENTE ','','2015-09-02','07:05:21',5),(1609,'CADASTRO DO CLIENTE ','','2015-09-02','07:05:36',5),(1610,'CADASTRO DO CLIENTE ','','2015-09-02','07:06:04',5),(1611,'CADASTRO DO CLIENTE ','','2015-09-02','07:06:17',5),(1612,'CADASTRO DO CLIENTE ','','2015-09-02','07:06:34',5),(1613,'ALTERAÇÃO DO CLIENTE ','','2015-09-04','00:22:55',5),(1614,'ALTERAÇÃO DO CLIENTE ','','2015-09-04','00:23:33',5),(1615,'ALTERAÇÃO DO CLIENTE ','','2015-09-04','00:23:48',5),(1616,'ALTERAÇÃO DO CLIENTE ','','2015-09-04','00:24:04',5),(1617,'ALTERAÇÃO DO CLIENTE ','','2015-11-18','20:09:06',3),(1618,'ALTERAÇÃO DO CLIENTE ','','2015-11-18','20:12:31',3),(1619,'ALTERAÇÃO DO CLIENTE ','','2015-11-18','20:15:22',3),(1620,'ALTERAÇÃO DO CLIENTE ','','2015-11-18','20:17:32',3),(1621,'ALTERAÇÃO DO CLIENTE ','','2015-11-18','20:20:26',3),(1622,'ALTERAÇÃO DO CLIENTE ','','2015-11-18','20:22:27',3),(1623,'CADASTRO DO CLIENTE ','','2015-11-18','20:24:37',3),(1624,'CADASTRO DO CLIENTE ','','2015-11-18','20:27:28',3),(1625,'ALTERAÇÃO DO CLIENTE ','','2015-11-18','20:29:07',3),(1626,'CADASTRO DO CLIENTE ','','2015-11-18','20:31:31',3),(1627,'CADASTRO DO CLIENTE ','','2015-11-18','20:33:17',3),(1628,'CADASTRO DO CLIENTE ','','2015-11-18','20:35:49',3),(1629,'CADASTRO DO CLIENTE ','','2015-11-18','20:38:10',3),(1630,'CADASTRO DO CLIENTE ','','2015-11-18','20:39:53',3),(1631,'CADASTRO DO CLIENTE ','','2015-11-18','20:44:25',3),(1632,'ALTERAÇÃO DO CLIENTE ','','2015-11-18','20:45:56',3),(1633,'CADASTRO DO CLIENTE ','','2015-11-18','20:47:55',3),(1634,'CADASTRO DO CLIENTE ','','2015-11-18','20:52:00',3),(1635,'CADASTRO DO CLIENTE ','','2015-11-18','20:54:14',3),(1636,'CADASTRO DO CLIENTE ','','2015-11-18','20:55:38',3),(1637,'ALTERAÇÃO DO CLIENTE ','','2015-11-18','20:58:38',3),(1638,'ALTERAÇÃO DO CLIENTE ','','2015-11-18','21:02:04',3),(1639,'ALTERAÇÃO DO CLIENTE ','','2015-11-18','21:02:47',3),(1640,'ALTERAÇÃO DO CLIENTE ','','2015-11-18','21:02:58',3),(1641,'ALTERAÇÃO DO CLIENTE ','','2015-11-18','21:03:08',3),(1642,'EXCLUSÃO DO LOGIN 9, NOME: , Email: ','DELETE FROM tb_clientes WHERE idcliente = \'9\'','2015-11-18','21:03:51',3),(1643,'EXCLUSÃO DO LOGIN 10, NOME: , Email: ','DELETE FROM tb_clientes WHERE idcliente = \'10\'','2015-11-18','21:03:57',3),(1644,'EXCLUSÃO DO LOGIN 11, NOME: , Email: ','DELETE FROM tb_clientes WHERE idcliente = \'11\'','2015-11-18','21:04:03',3),(1645,'EXCLUSÃO DO LOGIN 12, NOME: , Email: ','DELETE FROM tb_clientes WHERE idcliente = \'12\'','2015-11-18','21:04:08',3),(1646,'EXCLUSÃO DO LOGIN 13, NOME: , Email: ','DELETE FROM tb_clientes WHERE idcliente = \'13\'','2015-11-18','21:04:13',3),(1647,'EXCLUSÃO DO LOGIN 6, NOME: , Email: ','DELETE FROM tb_depoimentos WHERE iddepoimento = \'6\'','2015-11-18','21:04:34',3),(1648,'EXCLUSÃO DO LOGIN 7, NOME: , Email: ','DELETE FROM tb_depoimentos WHERE iddepoimento = \'7\'','2015-11-18','21:04:50',3),(1649,'ALTERAÇÃO DO CLIENTE ','','2015-11-18','21:05:35',3),(1650,'ALTERAÇÃO DO CLIENTE ','','2015-11-18','21:06:05',3),(1651,'ALTERAÇÃO DO CLIENTE ','','2015-11-18','21:06:41',3),(1652,'ALTERAÇÃO DO CLIENTE ','','2015-11-18','21:10:48',3),(1653,'ALTERAÇÃO DO CLIENTE ','','2015-11-23','11:56:17',5),(1654,'ALTERAÇÃO DO CLIENTE ','','2015-11-23','11:56:36',5),(1655,'ALTERAÇÃO DO CLIENTE ','','2015-11-23','11:58:49',5),(1656,'ALTERAÇÃO DO CLIENTE ','','2015-11-23','12:01:49',5),(1657,'ALTERAÇÃO DO CLIENTE ','','2015-11-25','11:38:28',5),(1658,'ALTERAÇÃO DO CLIENTE ','','2015-11-25','11:40:48',5),(1659,'ALTERAÇÃO DO CLIENTE ','','2015-11-25','11:42:20',5),(1660,'ALTERAÇÃO DO CLIENTE ','','2015-11-25','11:44:24',5),(1661,'DESATIVOU O LOGIN 46','UPDATE tb_dicas SET ativo = \'NAO\' WHERE iddica = \'46\'','2015-11-25','11:44:40',5),(1662,'EXCLUSÃO DO LOGIN 47, NOME: , Email: ','DELETE FROM tb_dicas WHERE iddica = \'47\'','2015-11-25','11:44:55',5),(1663,'EXCLUSÃO DO LOGIN 49, NOME: , Email: ','DELETE FROM tb_dicas WHERE iddica = \'49\'','2015-11-25','11:45:00',5),(1664,'DESATIVOU O LOGIN 48','UPDATE tb_dicas SET ativo = \'NAO\' WHERE iddica = \'48\'','2015-11-25','11:45:06',5),(1665,'CADASTRO DO CLIENTE ','','2015-11-25','12:01:24',5),(1666,'ALTERAÇÃO DO CLIENTE ','','2015-11-25','15:17:49',5),(1667,'ALTERAÇÃO DO CLIENTE ','','2015-11-25','15:18:57',5),(1668,'ALTERAÇÃO DO CLIENTE ','','2015-11-25','15:21:18',5),(1669,'ALTERAÇÃO DO CLIENTE ','','2015-11-25','15:22:43',5),(1670,'ALTERAÇÃO DO CLIENTE ','','2015-11-25','15:25:34',5),(1671,'ALTERAÇÃO DO CLIENTE ','','2015-11-25','15:26:45',5),(1672,'ALTERAÇÃO DO CLIENTE ','','2015-11-25','15:29:31',5),(1673,'ALTERAÇÃO DO CLIENTE ','','2015-11-25','15:31:46',5),(1674,'ALTERAÇÃO DO CLIENTE ','','2015-11-25','15:33:27',5),(1675,'ALTERAÇÃO DO CLIENTE ','','2015-11-25','15:34:51',5),(1676,'ALTERAÇÃO DO CLIENTE ','','2015-11-25','15:35:19',5),(1677,'DESATIVOU O LOGIN 35','UPDATE tb_banners SET ativo = \'NAO\' WHERE idbanner = \'35\'','2015-12-04','12:09:14',5),(1678,'DESATIVOU O LOGIN 34','UPDATE tb_banners SET ativo = \'NAO\' WHERE idbanner = \'34\'','2015-12-04','12:09:21',5),(1679,'ATIVOU O LOGIN 35','UPDATE tb_banners SET ativo = \'SIM\' WHERE idbanner = \'35\'','2015-12-04','12:55:08',5),(1680,'DESATIVOU O LOGIN 35','UPDATE tb_banners SET ativo = \'NAO\' WHERE idbanner = \'35\'','2015-12-04','15:46:22',5);
/*!40000 ALTER TABLE `tb_logs_logins` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_produtos`
--

DROP TABLE IF EXISTS `tb_produtos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_produtos` (
  `idproduto` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci NOT NULL,
  `title_google` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `keywords_google` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description_google` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ativo` varchar(3) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'SIM',
  `ordem` int(10) unsigned NOT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `id_categoriaproduto` int(10) unsigned NOT NULL,
  `marca` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `apresentacao` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `avaliacao` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `caracteristicas` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`idproduto`)
) ENGINE=InnoDB AUTO_INCREMENT=285 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_produtos`
--

LOCK TABLES `tb_produtos` WRITE;
/*!40000 ALTER TABLE `tb_produtos` DISABLE KEYS */;
INSERT INTO `tb_produtos` (`idproduto`, `titulo`, `imagem`, `descricao`, `title_google`, `keywords_google`, `description_google`, `ativo`, `ordem`, `url_amigavel`, `id_categoriaproduto`, `marca`, `apresentacao`, `avaliacao`, `caracteristicas`) VALUES (271,'Caldeira Horizontal 3CBT','1811201508092795222604..jpg','<p style=\\\"text-align: justify;\\\">\r\n	MARCA &ndash; SANTESSO</p>\r\n<p style=\\\"text-align: justify;\\\">\r\n	MODELO &ndash; VERS&Aacute;TIL &ndash; 3CBT</p>\r\n<p style=\\\"text-align: justify;\\\">\r\n	COMBUST&Iacute;VEL &ndash; LENHA, G&Aacute;S OU &Oacute;LEO.</p>\r\n<p style=\\\"text-align: justify;\\\">\r\n	UMA CALDEIRA GERADORA DE VAPOR - TIPO HORIZONTAL</p>\r\n<p style=\\\"text-align: justify;\\\">\r\n	FLAMOTUBULAR &ndash; FORNALHA TOTALMENTE SUBMERSA &ndash; COM GRELHAMENTO AQUOTUBOLAR &ndash; TR&Ecirc;S PASSAGENS DE FOGO (O FOGO PASSA TR&Ecirc;S VEZES PELA CALDEIRA EVITANDO PERDA DE CALORIA).</p>\r\n<p style=\\\"text-align: justify;\\\">\r\n	ISOLA&Ccedil;&Atilde;O T&Eacute;RMICA&nbsp;</p>\r\n<p style=\\\"text-align: justify;\\\">\r\n	MANTA DE L&Atilde; DE VIDRO &ndash; COM REVESTIMENTO MET&Aacute;LICO &ndash; PINTURA EM ALUM&Iacute;NIO INDUSTRIAL A.C..</p>\r\n<p style=\\\"text-align: justify;\\\">\r\n	PRESS&Atilde;O DE TRABALHO 120 LIBRAS 8.4 KLS/CM&sup2;</p>\r\n<p style=\\\"text-align: justify;\\\">\r\n	TESTE HIDROST&Aacute;TICO 180 LIBRAS 12.0 KLS/CM&sup2;</p>\r\n<p style=\\\"text-align: justify;\\\">\r\n	C&Oacute;DIGO DE FABRICA&Ccedil;&Atilde;O &ndash; ASME</p>\r\n<p style=\\\"text-align: justify;\\\">\r\n	CATEGORIA &ndash; &ldquo;B&rdquo;</p>','','','','SIM',0,'caldeira-horizontal-3cbt',49,NULL,NULL,NULL,NULL),(272,'Caldeira Horizontal - com ante fornalha','1811201508127545253518..jpg','<p>\r\n	MARCA &ndash; SANTESSO</p>\r\n<p>\r\n	MODELO &ndash; ANTE FORNALHA&nbsp;</p>\r\n<p>\r\n	COMBUST&Iacute;VEL &ndash; LENHA</p>\r\n<p>\r\n	UMA CALDEIRA GERADORA DE VAPOR - TIPO HORIZONTAL&nbsp;</p>\r\n<p>\r\n	COM ANTE FORNALHA &ndash; TR&Ecirc;S PASSAGENS DE FOGO &ndash; FORNALHA E GRELHAMENTO AQUOTUBULAR - ISOLA&Ccedil;&Atilde;O T&Eacute;RMICA EM MANTA DE L&Atilde; DE VIDRO &ndash; COM REVESTIMENTO MET&Aacute;LICO.</p>\r\n<p>\r\n	PRESS&Atilde;O DE TRABALHO 120 LIBRAS 8.4 KLS/CM&sup2;</p>\r\n<p>\r\n	TESTE HIDROST&Aacute;TICO 180 LIBRAS 12.0 KLS/CM&sup2;</p>\r\n<p>\r\n	C&Oacute;DIGO DE FABRICA&Ccedil;&Atilde;O &ndash; ASME</p>\r\n<p>\r\n	CATEGORIA &ndash; &ldquo;B&rdquo;</p>','','','','SIM',0,'caldeira-horizontal--com-ante-fornalha',49,NULL,NULL,NULL,NULL),(273,'Caldeira Horizontal - fornalha submersa','1811201508157937134483..jpg','<p>\r\n	MARCA &ndash; SANTESSO</p>\r\n<p>\r\n	MODELO &ndash; FORNALHA SUBMERSA</p>\r\n<p>\r\n	COMBUST&Iacute;VEL &ndash; LENHA</p>\r\n<p>\r\n	UMA CALDEIRA GERADORA DE VAPOR - TIPO HORIZONTAL&nbsp;</p>\r\n<p>\r\n	DUAS OU TR&Ecirc;S PASSAGENS DE FOGO &ndash; FORNALHA E GRELHAMENTO AQUOTUBULAR - ISOLA&Ccedil;&Atilde;O T&Eacute;RMICA EM MANTA DE L&Atilde; DE VIDRO &ndash; COM REVESTIMENTO MET&Aacute;LICO.</p>\r\n<p>\r\n	PRESS&Atilde;O DE TRABALHO 120 LIBRAS 8.4 KLS/CM&sup2;</p>\r\n<p>\r\n	TESTE HIDROST&Aacute;TICO 180 LIBRAS 12.0 KLS/CM&sup2;</p>\r\n<p>\r\n	C&Oacute;DIGO DE FABRICA&Ccedil;&Atilde;O &ndash; ASME</p>\r\n<p>\r\n	CATEGORIA &ndash; &ldquo;B&rdquo;</p>','','','','SIM',0,'caldeira-horizontal--fornalha-submersa',49,NULL,NULL,NULL,NULL),(274,'Caldeira Horizontal - a gás','1811201508174974547972..jpg','<p style=\\\"text-align: justify;\\\">\r\n	MARCA &ndash; SANTESSO</p>\r\n<p style=\\\"text-align: justify;\\\">\r\n	MODELO &ndash; QUEIMADOR A G&Aacute;S&nbsp;</p>\r\n<p style=\\\"text-align: justify;\\\">\r\n	COMBUST&Iacute;VEL &ndash; G&Aacute;S</p>\r\n<p style=\\\"text-align: justify;\\\">\r\n	UMA CALDEIRA GERADORA DE VAPOR - TIPO HORIZONTAL&nbsp;</p>\r\n<p style=\\\"text-align: justify;\\\">\r\n	UMA PASSAGEM DE FOGO - FLAMOTUBULAR- GRELHAMENTO AQUOTUBULAR</p>\r\n<p style=\\\"text-align: justify;\\\">\r\n	PRESS&Atilde;O DE TRABALHO 120 LIBRAS 8.4 KLS/CM&sup2;</p>\r\n<p style=\\\"text-align: justify;\\\">\r\n	TESTE HIDROST&Aacute;TICO 180 LIBRAS 12.0 KLS/CM&sup2;</p>\r\n<p style=\\\"text-align: justify;\\\">\r\n	C&Oacute;DIGO DE FABRICA&Ccedil;&Atilde;O &ndash; ASME</p>\r\n<p style=\\\"text-align: justify;\\\">\r\n	CATEGORIA &ndash; &ldquo;B&rdquo;</p>','','','','SIM',0,'caldeira-horizontal--a-gas',49,NULL,NULL,NULL,NULL),(275,'Caldeira Monobloco','1811201508201384835098..jpg','<p>\r\n	MARCA &ndash; SANTESSO</p>\r\n<p>\r\n	MODELO &ndash; MONOBLOCO&nbsp;</p>\r\n<p>\r\n	COMBUST&Iacute;VEL &ndash; LENHA</p>\r\n<p>\r\n	UMA CALDEIRA GERADORA DE VAPOR - TIPO HORIZONTAL&nbsp;</p>\r\n<p>\r\n	MONOBLOCO &ndash; &ldquo;FLAMOTUBULAR&rdquo; &ndash; FORNALHA NO SISTEMA REFRIGERADA &ndash; GRELHAMENTO AQUOTUBULAR - DUAS PASSAGENS DE FOGO</p>\r\n<p>\r\n	ISOLA&Ccedil;&Atilde;O T&Eacute;RMICA&nbsp;</p>\r\n<p>\r\n	MANTA DE L&Atilde; DE VIDRO &ndash; COM REVESTIMENTO MET&Aacute;LICO &ndash; PINTURA EM ALUM&Iacute;NIO INDUSTRIAL A.C..&nbsp;</p>\r\n<p>\r\n	PRESS&Atilde;O DE TRABALHO 120 LIBRAS 8.4 KLS/CM&sup2;</p>\r\n<p>\r\n	TESTE HIDROST&Aacute;TICO 180 LIBRAS 12.0 KLS/CM&sup2;</p>\r\n<p>\r\n	C&Oacute;DIGO DE FABRICA&Ccedil;&Atilde;O &ndash; ASME</p>\r\n<p>\r\n	CATEGORIA &ndash; &ldquo;B&rdquo;</p>','','','','SIM',0,'caldeira-monobloco',49,NULL,NULL,NULL,NULL),(276,'Caldeira Vertical','1811201508223621124914..jpg','<p>\r\n	MARCA &ndash; SANTESSO</p>\r\n<p>\r\n	MODELO &ndash; VERTICAL&nbsp;</p>\r\n<p>\r\n	COMBUST&Iacute;VEL &ndash; LENHA</p>\r\n<p>\r\n	UMA CALDEIRA GERADORA DE VAPOR - TIPO VERTICAL&nbsp;</p>\r\n<p>\r\n	UMA PASSAGEM DE FOGO - FLAMOTUBULAR- GRELHAMENTO AQUOTUBULAR- CONSTRU&Iacute;DA EM A&Ccedil;O ASTM-A-283-ESPESSURA 10,40MM, ESPELHOS 12,00MM- CAIXA DE FUMA&Ccedil;A E CHAMIN&Eacute; COM QUATRO METROS DE ALTURA BIPARTIDO &ndash; CHAMIN&Eacute; ACOPLADO &Agrave; CALDEIRA, ALTURA TOTAL &ndash; SEIS METROS.</p>\r\n<p>\r\n	PRESS&Atilde;O DE TRABALHO 120 LIBRAS 8.4 KLS/CM&sup2;</p>\r\n<p>\r\n	TESTE HIDROST&Aacute;TICO 180 LIBRAS 12.0 KLS/CM&sup2;</p>\r\n<p>\r\n	C&Oacute;DIGO DE FABRICA&Ccedil;&Atilde;O &ndash; ASME</p>\r\n<p>\r\n	CATEGORIA &ndash; &ldquo;B&rdquo;</p>','','','','SIM',0,'caldeira-vertical',49,NULL,NULL,NULL,NULL),(277,'Boiler aquecedor de água','1811201508275349550607..jpg','<div>\r\n	EQUIPAMENTO UTILIZADO PARA O R&Aacute;PIDO AQUECIMENTO DE &Aacute;GUA, DOTADO DE SISTEMA AUTOM&Aacute;TICO PARA O AQUECIMENTO E CONTROLE DA TEMPERATURA. SUA APLICA&Ccedil;&Atilde;O MAIS USUAL &Eacute; NA HIGIENIZA&Ccedil;&Atilde;O, ONDE H&Aacute; NECESSIDADE DE &Aacute;GUA ESTERILIZADA.</div>','','','','SIM',0,'boiler-aquecedor-de-agua',54,NULL,NULL,NULL,NULL),(278,'Vulcanizadoras','1811201508314691323778..jpg','<div>\r\n	COMPLETA COM TODOS OS ACESS&Oacute;RIOS: PURGADORES, FLEX&Iacute;VEIS, ISOLADA TERMICAMENTE COM MANTA DE L&Atilde; DE ROCHA E REVESTIMENTO MET&Aacute;LICO. CONSTRU&Iacute;DA EM A&Ccedil;O NO SISTEMA ATIRANTADO DE SEGURAN&Ccedil;A.</div>','','','','SIM',0,'vulcanizadoras',50,NULL,NULL,NULL,NULL),(279,'Tachos para doces','1811201508355408174966..jpg','<div>\r\n	TIPO BASCULANTE - PREPARADO PARA FABRICA&Ccedil;&Atilde;O DE TODOS OS TIPOS DE DOCES &ndash; &nbsp;ISOLADA TERMICAMENTE &nbsp;&ndash; &nbsp;CONSTRU&Iacute;DA EM A&Ccedil;O INOX &ndash; DOTADA DE UM MEXEDOR ACOPLADO A UM MOTOREDUTOR &ndash; SISTEMA DE SEGURAN&Ccedil;A COM V&Aacute;LVULA DE SEGURAN&Ccedil;A, MAN&Ocirc;METRO, FILTRO E PURGADOR.</div>','','','','SIM',0,'tachos-para-doces',55,NULL,NULL,NULL,NULL),(280,'Lavador de gases','1811201508395522995139..jpg','<div>\r\n	LAVADOR DE GASES COM UM EXAUSTOR DE BAIXA ROTA&Ccedil;&Atilde;O (3CV). QUE CIRCULA OS GASES PELO DUTO ACOPLADO AO LAVADOR ELIMINANDO TODA FULIGEM E AQUECIMENTO DO MEIO AMBIENTE. PROPORCIONA A LIMPEZA DOS GASES GERADO PELOS EQUIPAMENTOS A BASE DE COMBUST&Atilde;O ATRAV&Eacute;S DE VIA CIRCULAR &Uacute;MIDA, COM EFICI&Ecirc;NCIA DE 89% NA ELIMINA&Ccedil;&Atilde;O DOS CONTAMINANTES. CONSTRU&Ccedil;&Atilde;O EM A&Ccedil;O INOX AISI 304 E A&Ccedil;O ASTM-A-CR 420.</div>','','','','SIM',0,'lavador-de-gases',56,NULL,NULL,NULL,NULL),(281,'Filtro captador de fuligem','1811201508442040148356..jpg','<div>\r\n	TIPO VERTICAL, COLETOR DE PART&Iacute;CULAS COM DUTO, FABRICADO NO SISTEMA DE ACOPLAMENTO COM FLANGES E BOCA DE INSPE&Ccedil;&Atilde;O.</div>','','','','SIM',0,'filtro-captador-de-fuligem',51,NULL,NULL,NULL,NULL),(282,'Exaustores','1811201508471614933618..jpg','<div>\r\n	NOSSOS EXAUSTORES EST&Atilde;O ENTRE OS MELHORES DO MERCADO, FABRICADOS EM DIVERSAS DIMENS&Otilde;ES E USADOS EM V&Aacute;RIOS SEGMENTOS, INDUSTRIAIS OU N&Atilde;O, ONDE &Eacute; NECESS&Aacute;RIO VENTILA&Ccedil;&Atilde;O NO RECINTO.</div>','','','','SIM',0,'exaustores',52,NULL,NULL,NULL,NULL),(283,'Fornalhas','1811201508515602620123..jpg','<div>\r\n	TIPO MONOBLOCO PARA ACOPLAMENTO EM CALDEIRAS, UTILIZA&Ccedil;&Atilde;O NO SISTEMA MISTO, ONDE SE PERMITE A QUEIMA DESDE LENHA A CEREAIS PEREC&Iacute;VEIS. TEM A FINALIDADE DE AUMENTAR A PRODU&Ccedil;&Atilde;O DE VAPOR DA CALDEIRA NO QUAL EST&Aacute; ACOPLADO.</div>','','','','SIM',0,'fornalhas',53,NULL,NULL,NULL,NULL),(284,'Queimador para Glicerol','1811201508551572635902..jpg','<p>\r\n	A CALDEIRA SANTESSO APRESENTA UMA INOVA&Ccedil;&Atilde;O DE SUA AUTORIA E QUE TEM A MISS&Atilde;O DE LEVAR ECONOMIA, NO CONSUMO DE LENHA NA CALDEIRA, FORNO OU FORNALHA DA VOSSA EMPRESA.</p>\r\n<p>\r\n	TRATA-SE DE UM QUEIMADOR PARA GLICEROL, UM COMBUST&Iacute;VEL PROVENIENTE DO SUBPRODUTO VEGETAL OU ANIMAL DE BAIXO CUSTO E PODER CALOR&Iacute;FICO DE 4.000KCAL, ACARRETANDO UMA ECONOMIA DE 70% NO CONSUMO DE LENHA DA CALDEIRA.</p>\r\n<p>\r\n	O SISTEMA &Eacute; COMPOSTO DE QUEIMADOR TIPO MONOBLOCO, COM TANQUE DE SERVI&Ccedil;O DE NO M&Aacute;XIMO 2000 LITROS COM SERPENTINA, MOTO BOMBA E FILTRO AL&Eacute;M DE OUTROS ACESS&Oacute;RIOS QUE COMP&Otilde;EM O CONJUNTO</p>\r\n<p>\r\n	COMPARA&Ccedil;&Atilde;O DE PODER CALOR&Iacute;FICO:</p>\r\n<p>\r\n	- LENHA, EUCALIPTO - 3.500 KCAL.</p>\r\n<p>\r\n	- ECO &Oacute;LEO - 4.250 KCAL.</p>\r\n<p>\r\n	AL&Eacute;M DE ECONOMIA, LEVAMOS SOLU&Ccedil;&Atilde;O COM OS PROBLEMAS JUNTO AOS &Oacute;RG&Atilde;OS DO MEIO AMBIENTE.&nbsp;</p>\r\n<p>\r\n	SUA EMPRESA ECONOMIZA, E A NATUREZA AGRADECE!.</p>','','','','SIM',0,'queimador-para-glicerol',57,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `tb_produtos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_servicos`
--

DROP TABLE IF EXISTS `tb_servicos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_servicos` (
  `idservico` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci,
  `imagem` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'imagem_nao_disponivel.jpg',
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_categoriaservico` int(10) unsigned NOT NULL,
  `imagem_1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`idservico`)
) ENGINE=MyISAM AUTO_INCREMENT=41 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_servicos`
--

LOCK TABLES `tb_servicos` WRITE;
/*!40000 ALTER TABLE `tb_servicos` DISABLE KEYS */;
INSERT INTO `tb_servicos` (`idservico`, `titulo`, `descricao`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `title_google`, `keywords_google`, `description_google`, `id_categoriaservico`, `imagem_1`, `imagem_2`) VALUES (36,'REFORMAS','<p>\r\n	REFORMAMOS CALDEIRAS, TANQUES SECADORES DE SANGUE, TANQUES SECADORES DE FARINHA DE ORIGEM ANIMAL ETC.</p>\r\n<p>\r\n	APROVEITAMOS SOMENTE O CORPO DOS EQUIPAMENTO E FAZEMOS UMA TROCA TOTAL DAS TUBULA&Ccedil;&Otilde;ES E ACESS&Oacute;RIOS EXISTENTES NO EQUIPAMENTO, OFERECENDO GARANTIA DE ATE 12 MESES PARA OS NOSSOS SERVI&Ccedil;OS.</p>','1811201509105351497477..jpg','SIM',NULL,'reformas','','','',0,'',''),(37,'MONTAGEM E INSTALAÇÃO','<div>\r\n	OFERECEMOS UM PROJETO COMPLETO DE MONTAGEM PARA SUA IND&Uacute;STRIA, PARA QUE POSSAMOS LEVAR AOS NOSSOS CLIENTES, UM MELHOR APROVEITAMENTO DA &Aacute;GUA, VAPOR E COMBUST&Iacute;VEL, GERANDO AL&Eacute;M DE ECONOMIA ENERG&Eacute;TICA, UM CONSUMO MAIS CONSCIENTE.</div>','2311201511569809989560..jpg','SIM',NULL,'montagem-e-instalacao','','','',0,'',''),(38,'TUBULAÇÕES','<div>\r\n	COMERCIALIZAMOS PRODUTOS DA MAIS ALTA QUALIDADE, CAPAZ DE ATENDER &Agrave;S MAIS R&Iacute;GIDAS EXIG&Ecirc;NCIAS DE NOSSOS CLIENTES, EM TODOS OS SEGMENTOS ONDE ATUAMOS COM O OBJETIVO DE GARANTIR AGILIDADE E CONFIABILIDADE PARA O FORNECIMENTO DE NOSSAS TUBULA&Ccedil;&Otilde;ES. O MIX DE PRODUTOS INCLUI OS A&Ccedil;OS COM ACABAMENTOS: DECAPADO, POLIDO, GALVANIZADO, ESCOVADO ETC.</div>','2311201511583192328268..jpg','SIM',NULL,'tubulacoes','','','',0,'',''),(39,'ISOLAMENTO TÉRMICO','<div>\r\n	ATENDENDO &Agrave;S NECESSIDADES DA SUA EMPRESA, A CALDEIRAS SANTESSO POSSUI PROFISSIONAIS ESPECIALIZADOS NO DESENVOLVIMENTO DE SERVI&Ccedil;OS E APLICA&Ccedil;&Otilde;ES DE MATERIAIS REFRAT&Aacute;RIOS E ISOLANTES EM FORNOS INDUSTRIAIS, ESTUFAS, CALDEIRAS, TUBULA&Ccedil;&Otilde;ES E DEMAIS EQUIPAMENTOS T&Eacute;RMICOS, BEM COMO SERVI&Ccedil;OS E MANUTEN&Ccedil;&Otilde;ES PREVENTIVAS E CORRETIVAS.</div>','2311201512012935954063..jpg','SIM',NULL,'isolamento-termico','','','',0,'',''),(40,'INSPEÇÕES','<div>\r\n	FAZEMOS ANUALMENTE OU EM CONDI&Ccedil;&Otilde;ES EXTRAORDIN&Aacute;RIAS, UM LEVANTAMENTO PRECISO DAS CONDI&Ccedil;&Otilde;ES DA CALDEIRA OU VASOS DE PRESS&Atilde;O, PARA REGULAMENTAR, JUNTO AO CREA, TODA DOCUMENTA&Ccedil;&Atilde;O NECESS&Aacute;RIA PARA O FUNCIONAMENTO LEGAL DO EQUIPAMENTO.</div>','2511201512011732562548..jpg','SIM',NULL,'inspecoes','','','',0,'','');
/*!40000 ALTER TABLE `tb_servicos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'masmidia_caldeira_santesso'
--

--
-- Dumping routines for database 'masmidia_caldeira_santesso'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-07 15:47:49
